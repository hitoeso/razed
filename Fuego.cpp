/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fuego.cpp
 * Author: Raquel
 * 
 * Created on 9 de mayo de 2016, 16:41
 */

#include "Fuego.h"
#define PI 3.14159265

Fuego::Fuego() {
}

Fuego::Fuego(int bx, int by, float x, float y, double mouseX, double mouseY, sf::Texture *tex, int boundx)  : Colisionable(bx, by){
    
    clock2 = new sf::Clock;
    
    sprite = new sf::Sprite(*tex);
    sprite->setOrigin(32/2, 32/2);
    sprite->setTextureRect(sf::IntRect(13*32, 8*32, 2*32+8, 88));
    sprite->setScale(0.35, 0.35);
    sprite->setPosition(x-boundx, y);
    
    float angx = mouseX-x;
    float angy = mouseY-y;
    float mod = sqrt(angy*angy+angx*angx);
    dirx = angx/mod;
    diry = angy/mod; 
    sprite->setRotation(atan2(angy, angx) * 180 / PI);
    
    posX = x;
    posY = y;
    
    vida=2.1;
    time = 0; 
    timeExpl=0;
    numframe = 0;  
}

bool Fuego::update(std::vector<Colisionable*>* c){
    elapsed = new sf::Time(clock2->getElapsedTime());
    time = (elapsed->asSeconds());
    //delete elapsed;
   if(time < vida){
        if(0.7 > time && time > 0.3){
            sprite->setTextureRect(sf::IntRect(10*32+34, 8*32, 8*8, 88));
        }
        else if(time > 0.7){
            sprite->setTextureRect(sf::IntRect(9*32, 8*32, 7*8, 88));
        }
        return true;
    }
    else{
        this->destroy();
        return false; 
    }
}
void Fuego::render(){
    sprite->setPosition(posX,posY);
}
Fuego::~Fuego() {
}

void Fuego::destroy() {
    delete clock2;
    delete sprite;
    delete this;
}
