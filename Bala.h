/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bala.h
 * Author: Raquel
 *
 * Created on 10 de marzo de 2016, 15:04
 */

#ifndef BALA_H
#define BALA_H
#include "Npc.h"
#include "Colisionable.h"

#include <iostream>
#include <SFML/Graphics.hpp>

class Bala : public Colisionable {
public:
    Bala();
    Bala(int bx, int by, float x, float y, float velocidad, float life, float fuerza, double mouseX, double mouseY, sf::Texture *tex, int type);
    ~Bala();
    sf::Sprite getSprite();
    void destroy();
    bool update(sf::Time *deltaTime);
    void render();
    float getDanio();
private:

    float posX;
    float posY;
    float inicialX;
    float inicialY;

    float dirx;
    float diry;
   
    float vel;
    float vida;
    
    float time;
    int danio;
    sf::Clock *clock2;
    sf::Time *elapsed;
};

#endif /* BALA_H */

