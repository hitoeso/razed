/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Colisionable.h
 * Author: Raquel
 *
 * Created on 16 de marzo de 2016, 16:00
 */

#ifndef COLISIONABLE_H
#define COLISIONABLE_H
#include <iostream>
#include <SFML/Graphics.hpp>

class Colisionable {
public:
    Colisionable();
    Colisionable(int bx, int by);
    virtual ~Colisionable();
    bool colisionar(Colisionable *obj);
    sf::Sprite* getSprite();
    sf::Vector2i getBounding();
    void destroy();
    float getPosX();
    float getPosY();
    
protected:
    int boundX;
    int boundY;
    sf::Sprite *sprite;
    
};

#endif /* COLISIONABLE_H */

