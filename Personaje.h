/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   personaje.h
 * Author: Raquel
 *
 * Created on 18 de abril de 2016, 16:37
 */

#ifndef PERSONAJE_H
#define PERSONAJE_H

#include <iostream>
#include <SFML/Graphics.hpp>

#include "Colisionable.h"
#include "Bala.h"
#include "Bomba.h"
#include "Fuego.h"
#include "Arma.h"
#include "Juego.h"
#include "Pared.h"

class Personaje: public Colisionable  {
public:
    Personaje();
    Personaje(float bx, float by, float x, float y, sf::Texture *tex);
    void setVida(float v);
    void setFuerza(float f);
    void setVelocidad(float vel);
    int radiacion;
    void lookAtMouse();
    
    /*Ultimates*/
    void tirarGranada(sf::Texture *tex, std::vector<Colisionable*>* g);
    void rastroFuego(sf::Texture *tex, std::vector<Colisionable*>* g, float);
    /**/
    
    void cogerPocion(int tipo);
    sf::Sprite* getSprite(); 
    float getvida();
    float getvidamax();
    int getradiacion();
    Arma* getArma();
    bool getPocion2();
    bool getPocion3();
    int getDisponibles();
    float times2;
    float getPosX();
    float getPosY();

    bool getinvulnerable();
    bool setinvulnerable(int i);
    
    bool update(float sec, sf::Texture *tex, std::vector<Colisionable*> *b, std::vector<Colisionable*> *p, sf::Time *deltaTime);
    void render(float sec);

    ~Personaje();
    void destroy();
private:
    sf::Vector2i position;
    Arma *arma;
    Juego *juego;
    
    float vida;
    float vidamax;
    float fuerza;
    int velocidad;
    int velocidadPocion;
    
    int numeroDeFrames;
    int sentido;
    
    int contAnim;
    
    float posXC;
    float posYC;
    float kVel;
    int velPocion;
    
    int fuego;
    bool granadaDisponible;
    bool invulnerable;
    float timebomba;
    float timefuego;
    int disponibles;
    float fPocion;
    
    double mouseX;
    double mouseY;
    
    float tVel;
    float tFuerza;
    sf::Clock *clockVel;
    sf::Time *timeVel;
    sf::Time *timeFuerza;
    bool pocion2;
    bool pocion3;
    
    float time;
    sf::Clock *clock2;
    sf::Time *elapsed;
    
    int time3;
    int timerad;
    sf::Clock *clock3;
    sf::Time *elapsed3;
    
    bool rightPressed;
};

#endif /* PERSONAJE_H */



