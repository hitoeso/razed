/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arma.h
 * Author: Pablo
 *
 * Created on 19 de abril de 2016, 19:22
 */

#ifndef ARMA_H
#define ARMA_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Juego.h"

class Arma {
public:
    Arma(int tp, float f, float v, float cad, float tb, int carg, int tc);
    void setFuerza(float f);
    float getFuerza();
    void setCadencia(float c);
    float getCadencia();
    void setTimeBala(float t);
    float getTimeBala();
    void setVelocidadBala(float v);
    float getVelocidadBala();
    void setNumBalas(int n);
    int getNumBalas();
    int getCargador();
    void setCargador(int c);
    int getTipo();
    void update();
    ~Arma();
    void destroy();
private:
    Juego *juego;
    int fuerza;
    float timebala;
    float cadencia;
    float vel;
    int cargador;
    int numBalas;
    int tCarga;
    sf::Clock clock2;
    sf::Time elapsed;
    float time;
    int tipo;
};

#endif /* ARMA_H */