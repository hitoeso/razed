/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   personaje.cpp
 * Author: Raquel
 * 
 * Created on 18 de abril de 2016, 16:37
 */
#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <math.h>
#include "Personaje.h"

using namespace std;

Personaje::Personaje(float bx, float by, float x, float y, sf::Texture *tex) : Colisionable(bx, by){  
    juego = Juego::Instance();
    
    sprite = new sf::Sprite(*tex);
    sprite->setOrigin(32/2, 32/2);
    if(juego->getPersonaje()==0)
        sprite->setTextureRect(sf::IntRect(32, 0*32, 32, 32));
    else if(juego->getPersonaje()==1)
        sprite->setTextureRect(sf::IntRect(7*32, 0*32, 32, 32));
    
    if(juego->getPersonaje()==0){
        vida=100;
        vidamax=100;
    }
    else if(juego->getPersonaje()==1){
        vida=100/2;
        vidamax=100/2;
    }
    
    if(juego->getPersonaje()==0){
        velocidad = 3000;
    }
    else if(juego->getPersonaje()==1){
        velocidad = 3000*1.25;
    }
    
    velocidadPocion = 0;
    sprite->setPosition(x, y);
    radiacion=0;
    invulnerable=false;
    posXC = 300;
    posYC = 300;
    kVel = 0.03;
    disponibles = 3;
    numeroDeFrames = 5;
    sentido = 1;
    times2=0;
    contAnim = 3;
    timebomba=0;
    fuerza=0;
    //CREAR ARMA DEL PERSONAJE
    /*  TIPOS  
        ARMA 1 
        arma = new Arma(1, 2.0, 1000.0, 0.3, 0, 5, 2);
        TIPO, fuerza, velocidad, cadencia, timebala, cargador, tiempo de carga
     
        ARMA 2
        arma = new Arma(1, 1.0, 1000.0, 0.1, 0, 5, 1);
     
     */
    
    arma = new Arma(1, 1, 1000.0, 0.1, 0, 50, 2);
    
    time = 0;   
    clock2 = new sf::Clock();
    fuego=0;
    
    time3 = 0;   
    timerad=0;
    clock3 = new sf::Clock();
    
    clockVel = new sf::Clock();
    pocion2=false;
    pocion3=false;
    tVel = 0;
    tFuerza = 0;
    fPocion = 0;
    timefuego=0;
}

sf::Sprite* Personaje::getSprite(){
    return sprite;
}

void Personaje::cogerPocion(int tipo){

    if(tipo == 1){
        this->setVida(40);

    }
    else if(tipo == 2){
        velocidadPocion = 2000;
        pocion2 = true;
        timeVel = new sf::Time(clockVel->getElapsedTime());
        tVel = timeVel->asSeconds();

    }
    else if(tipo == 3){
        fPocion = 2.0;
        pocion3 = true;
        timeFuerza = new sf::Time(clockVel->getElapsedTime());
        tFuerza = timeFuerza->asSeconds();
    }
}

Personaje::~Personaje() {

}

void Personaje::destroy() {
    arma->destroy();
    delete arma;
    delete clockVel;
    delete timeVel;
    delete timeFuerza;
    delete clock2;
    delete elapsed;
    delete clock3;
    delete elapsed3;
    delete this;
}

void Personaje::setVida(float v){
    vida = vida+v;
    if(vida<=0){
        vida=0;
    }
    if(vida>vidamax){
        vida=vidamax;
    }
    if(v<0)
       juego->sonidoGrito();

}

int Personaje::getDisponibles(){
    return disponibles;
}

bool Personaje::getinvulnerable(){
    return invulnerable;
}

bool Personaje::setinvulnerable(int i){
    if(i==0){invulnerable=true; elapsed = new sf::Time(clock2->restart());time=0;}
    if(i==1){invulnerable=false;}
   
}

void Personaje::setFuerza(float  f){
    fuerza = f;
}

void Personaje::setVelocidad(float vel){
    velocidad = vel;
}

float Personaje::getvida(){
    return vida;
}

float Personaje::getvidamax(){
    return vidamax;
}

int Personaje::getradiacion(){
    return radiacion;
}
void Personaje::render(float sec){
    //std:: cout << sec <<std::endl;
    //std:: cout << times2 <<std::endl;
    if(sec-times2>0.15){
         //sprite->setPosition(posXC, posYC);
        if(juego->getPersonaje()==0){
            sprite->setTextureRect(sf::IntRect(contAnim*32, 0*32, 32, 30));
        }
        else if(juego->getPersonaje()==1){
            sprite->setTextureRect(sf::IntRect((contAnim+7)*32, 0*32, 32, 30));
        }
        if(sentido==1)contAnim++;
        if(sentido==-1)contAnim--;
        if(contAnim == numeroDeFrames || contAnim == 1) sentido=sentido*-1;
        
         times2=sec;  
    }
}

void Personaje::lookAtMouse()
{
    mouseX = juego->getMouseX();
    mouseY = juego->getMouseY();
    
    float dx = posXC - mouseX;
    float dy = posYC - mouseY;

    float rotation = (atan2(dy, dx)) * 180 / 3.14159265;

    sprite->setRotation(rotation+180);
}

bool Personaje::update(float sec, sf::Texture *tex, std::vector<Colisionable*> *b, std::vector<Colisionable*> *p, sf::Time *deltaTime){
    //std::cout<<getPosX()<<"  "<<getPosY()<<std::endl;
    if(vida>0){
        timeVel = new sf::Time(clockVel->getElapsedTime());
        timeFuerza = new sf::Time(clockVel->getElapsedTime());

        if(pocion2 == true && timeVel->asSeconds()-tVel>=10){
            pocion2 = false;
            velocidadPocion = 0;
        }
        if(pocion3 == true && timeFuerza->asSeconds()-tFuerza>=10){
            pocion3 = false;
            fPocion = 0;
        }

            elapsed3 = new sf::Time(clock3->getElapsedTime());
            time3 = elapsed3->asSeconds();
        lookAtMouse();
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
            float x = 0;
            float y = 0;
            if(sprite->getPosition().x < 1600){
               if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
                    //Escala por defecto
                    x = kVel*(velocidad+velocidadPocion)*deltaTime->asSeconds();
                    sprite->setPosition(posXC, posYC);  
                    if(time3!=timerad){
                        radiacion=radiacion-10;
                        if(radiacion<=0){
                             radiacion=0;
                        }
                    timerad=time3;
                    }
               }    
            }
            if(sprite->getPosition().x > 0){
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){

                    //Reflejo vertical
                    x = -kVel*(velocidad+velocidadPocion)*deltaTime->asSeconds();
                    sprite->setPosition(posXC, posYC); 
                    if(time3!=timerad){
                        radiacion=radiacion-10;
                        if(radiacion<=0){
                             radiacion=0;
                        }
                    timerad=time3;
                    }
                }
            }
            if(sprite->getPosition().y > 0){
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
                    y = -kVel*(velocidad+velocidadPocion)*deltaTime->asSeconds();;
                    sprite->setPosition(posXC, posYC);  
                    if(time3!=timerad){
                        radiacion=radiacion-10;
                        if(radiacion<=0){
                             radiacion=0;
                        }
                    timerad=time3;
                    }
                }
            }
            if(sprite->getPosition().y < 1600){
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                     y = kVel*(velocidad+velocidadPocion)*deltaTime->asSeconds();;
                    sprite->setPosition(posXC, posYC);  
                    if(time3!=timerad){
                        radiacion=radiacion-10;
                        if(radiacion<=0){
                             radiacion=0;
                        }
                    timerad=time3;
                    }
                }

            }
            bool casa = false;
            for(int o = 0; o<p->size(); o++){
                Pared *pared = dynamic_cast <Pared*>(p->at(o));
                if(pared->getPX()-20 < x + posXC && pared->getPX() + pared->getWidth()+20 > x+posXC && pared->getPY()-20 < y+posYC && pared->getPY()+pared->getHeight()+20>y+posYC ){
                   casa = true; 
                }  
            }   
            if(!casa){
                posXC += x;
                posYC += y;
                x=0;
                y=0;
                sprite->setPosition(posXC, posYC);
            }     
        }else{
            if(juego->getPersonaje()==0)
                sprite->setTextureRect(sf::IntRect(32, 0*32, 32, 32));
            else if(juego->getPersonaje()==1)
                sprite->setTextureRect(sf::IntRect(7*32, 0*32, 32, 32));         
            if(time3!=timerad){
                radiacion=radiacion+20;
                if(radiacion>=100){
                    radiacion=100;
                    this->setVida(-10);
                }
                timerad=time3;
            }
        }

        /* ------------------- ARMAS Y BALAS DEL PERSONAJE ------------------------ */
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
             if(juego->getPersonaje()==0)
                sprite->setTextureRect(sf::IntRect(32*3, 1*32, 32, 30));
            else if(juego->getPersonaje()==1)
                sprite->setTextureRect(sf::IntRect(32*9, 1*32, 32, 30)); 
            kVel=0.015;
               if( (sec-arma->getTimeBala()>=arma->getCadencia()) && arma->getCargador()>0){
                   juego->sonidoDisparo();
                   //COMPROBAR TIPO DE ARMA PARA TIRAR MAS O MENOS BALAS
                   if(juego->getPersonaje() == 0){
                        b->push_back(new Bala(4, 4, posXC, posYC, arma->getVelocidadBala(), 5, arma->getFuerza()+fuerza+fPocion, mouseX, mouseY, tex,0));
                   }
                   else if(juego->getPersonaje() == 1){ //generar tres balas con distinto angulo
                        b->push_back(new Bala(4, 4,  posXC, posYC, arma->getVelocidadBala(), arma->getTimeBala(), arma->getFuerza(), mouseX, mouseY, tex,0));
                   }
                   
                   std::cout<<arma->getFuerza()<<"golaaaaaaaaaaa"<<std::endl;
                    arma->setCargador(-1);
                    arma->setTimeBala(sec);
               }
        }
        else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)){
            if(juego->getPersonaje()==0){
                if(sec-timebomba>0.5){   
                    tirarGranada(tex, b);
                    timebomba=sec;
                }
            }
            else if(juego->getPersonaje()==1){
                
                if(fuego<17){
                    std::cout << "arriba" << fuego << std::endl;
                    std::cout << "disponibles" << disponibles << std::endl;
                    rastroFuego(tex, b, sec);
                }
                else{
                    if(sec-timefuego>0.5){
                        fuego=0;
                    }
                }
                if(fuego == 0 && disponibles >0){
                    disponibles--;
                    std::cout << fuego << std::endl;
                }
            }

        }else{
            rightPressed=false;
            kVel=0.03;
        }
        if(time < 0.5){
            elapsed = new sf::Time(clock2->getElapsedTime());
            time = elapsed->asSeconds();

            if(time>0.4){
                invulnerable=false;
            }
        }
        if(radiacion>=50){
        this->setFuerza((radiacion/10)+fPocion);
        }else{
            this->setFuerza(0+fPocion);
        }

        return true;
    }
    else{
        times2=0;
        return false;
    }
}

void Personaje::tirarGranada(sf::Texture *tex, std::vector<Colisionable*>* g){

    if(disponibles>0){
        juego->sonidoGranada();
        juego->sonidoFireinthehole();
        g->push_back(new Bomba(64, 64, sprite->getPosition().x, sprite->getPosition().y, mouseX, mouseY, tex));
        disponibles--;
    }
    granadaDisponible = true; 
}

void Personaje::rastroFuego(sf::Texture *tex, std::vector<Colisionable*>* g, float sec){
    if(disponibles>0 && fuego<=17){
        if(sec-timefuego>0.1){
            juego->sonidoFireinthehole();
            g->push_back(new Fuego(16, 16, sprite->getPosition().x, sprite->getPosition().y, mouseX, mouseY, tex, Colisionable::getBounding().x));
            timefuego=sec;
            fuego++;

        }
    }
    granadaDisponible = true; 
}

float Personaje::getPosX(){
    return posXC;
}

float Personaje::getPosY(){
    return posYC;
}

bool Personaje::getPocion2(){
    return pocion2;
}

bool Personaje::getPocion3(){
    return pocion3;
}

Arma* Personaje::getArma(){
    return arma;
}