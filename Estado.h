/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Estado.h
 * Author: Raquel
 *
 * Created on 18 de abril de 2016, 14:30
 */

#ifndef ESTADO_H
#define ESTADO_H

class Estado {
public:
    Estado();
    virtual ~Estado();
    void setStatus();
    void update();
    void draw();
    void leaving();
    void destroy();
private:
    
};

#endif /* ESTADO_H */

