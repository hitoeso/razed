/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Ingame.h
 * Author: Raquel
 *
 * Created on 18 de abril de 2016, 14:48
 */

#ifndef INGAME_H
#define INGAME_H
#include <iostream>
#include <SFML/Graphics.hpp>

#include "Juego.h"
#include "Estado.h"
#include "Personaje.h"
#include "Colisionable.h"
#include "Bala.h"
#include "Arma.h"
#include "Bomba.h"
#include "Fuego.h"
#include "Pared.h"
#include "Pocion.h"
#include "Personaje.h"
#include "Npc.h"

#include "tinyxml.h"
#include "mapa.h"


class Ingame : public Estado {
public:
    static Ingame* Instance();
    void setStatus(sf::Texture *tex, sf::Texture*, sf::Texture *hud, sf::Texture *bajavida, sf::Texture *dmg, sf::Font *fuente);
    bool getIniciado();
    bool getPausa();
    void setPausa();
    void update();
    void draw();
    void gameOver();
    void destroy();
protected:
    Ingame();
    /*Ingame();*/

    virtual ~Ingame();
    void generarNpc();

private:
    static Ingame* pinstance;
    
    bool iniciado;
    bool pausa;
    Juego *juego;
    std::vector<Colisionable*> c;
    std::vector<Colisionable*> p;
    mapa *Mapa;
    Personaje *pers;
    //próximamente un string con la url del sprite sheet
    sf::Texture *textura;
    sf::Texture *texturahudbars;
    sf::Texture *texturahud;
    sf::Texture *bajavida;
    sf::Texture *texturadmg;
    //tiempo de ejecución
    float seconds;
    //tiempo de generacion de NPC's y pociones
    float tGenNPC; 
    float tGenPocion;
    int contpoc;
    int numZombis;
    int totalZombis;
    int zombisGenerados;
    float dificultad;
    int nivel;
    int tipoNPC;
    bool bossCreado;
    int muertos;
    float tiempoRonda;
    float ronda;
    bool tiempo;

    sf::Clock *deltaClock;
    sf::Time *deltaTime;
    sf::Clock *cambioRondaClock;
    sf::Time *cambioRondaTime;
    
    sf::Clock *clock;
    sf::Time *time;
    
    sf::Clock *clockRonda;
    sf::Time *rondaTime;
    
    sf::Sprite *sprite;
    
    sf::View *view;
    sf::View *view2;
    sf::Sprite *cara;
    sf::Sprite *sprite2;
    sf::Sprite *sprite3;
    sf::Sprite *bajahp;
    sf::Sprite *extradmg;
    sf::Sprite *pocion2;
    sf::Sprite *pocion3; 
    
    /*Contador balas y granadas*/
    sf::Text *texto;
    sf::Text *texto2;
    sf::Text *texto3;
    sf::Text *texto4;
    bool noche;
    char string[10];
    char string2[10];
    char string3[10];
    char string4[10];

    sf::Font *fuente;
    sf::Sprite *balas;
    sf::Sprite *granadas;
    /**/
    
    bool gameover;
    
};

#endif /* INGAME_H */

