/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Menu.h
 * Author: Raquel
 *
 * Created on 18 de abril de 2016, 14:48
 */

#ifndef MENU_H
#define MENU_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include "vector"
#include <cstddef>
#include <string>

#include "Estado.h"
#include "Juego.h"

class Menu : public Estado{
public:
    static Menu* Instance();
    void setStatus(int type, float sx, float sy);
    void draw();
    void update(float sx, float sy);
    void click(float sx, float sy);
protected:
    Menu();
    virtual ~Menu();
    void leaving();
    void changeType(int i);    
    void destroy();
    void click();
    
    std::vector<sf::Sprite*> getSprites();
private:
    static Menu* pinstance;
    
    Juego *juego;
    sf::Sprite* fondo;
    
    std::vector<sf::Sprite*> botones;
    //Para el menu de Opciones
    std::vector<sf::Sprite*> barras;
    //Para textos
    std::vector<sf::Sprite*> texto;
    //Para los sprites de armas (el último del array es el cuadro de seleccion)
    std::vector<sf::Sprite*> armas;
    //Para los sprites de personajes (el último del array es el cuadro de seleccion)
    std::vector<sf::Sprite*> personajes;
    
    std::vector<sf::Sprite*> tarjeta;
    
    sf::Texture *tSelec;
    sf::Texture *tMenu;
    sf::Texture *tFondo0;
    sf::Texture *tFondo1;
    sf::Texture *tFondo2;
    sf::Texture *tFondo3;
    
    float times;
    sf::Clock *clock;
    sf::Time *elapsed;
    
    int type;
    bool musica;
    bool sonido;
    
    int animGrande;
    int sentidoGrande;
    int animPeque;
    int sentidoPeque;
    
    void updateTarjeta(int, int, int); 
    void updatePersonaje(int, int, int, int);
    
    void updateBoton(sf::Sprite *spr, int i, float sx, float sy);
    void updateBoton2(sf::Sprite *spr, int i, float sx, float sy);
    
    void updateSonido(sf::Sprite *spr, float sx, float sy);
    void updateMusica(sf::Sprite *spr, float sx, float sy);
    
    void cambiarVolumenMusica(sf::Sprite *spr, sf::Sprite *spr2, sf::Sprite *spr3, float sx, float sy);
    void cambiarVolumenEfectos(sf::Sprite *spr, sf::Sprite *spr2, sf::Sprite *spr3, float sx, float sy);
};

#endif /* MENU_H */

