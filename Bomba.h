/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bomba.h
 * Author: Raquel
 *
 * Created on 14 de marzo de 2016, 15:04
 */

#ifndef BOMBA_H
#define BOMBA_H

#include <SFML/Graphics.hpp>
#include "Colisionable.h"

class Bomba: public Colisionable{
public:
    Bomba();
    Bomba(int bx, int by, float x, float y, double mouseX, double mouseY, sf::Texture *tex);
    ~Bomba();
    sf::Sprite getSprite();
    void destroy();
    bool update(std::vector<Colisionable*>* c);
    void render();
private:
    float posX;
    float posY;
    float inicialx;
    float inicialy;
    float mouseX;
    float mouseY;
    float dirx;
    float diry;
    int vida;
    int vel;
    int numframe;
    bool explosion;
    float time;
    float timeExpl;
    
    sf::Clock *clock2;
    sf::Time *elapsed;
};

#endif /* BOMBA_H */

