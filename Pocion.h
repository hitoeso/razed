/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pocion.h
 * Author: Carmina
 *
 * Created on 6 de mayo de 2016, 11:42
 */

#ifndef POCION_H
#define POCION_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Colisionable.h"

class Pocion : public Colisionable {
public:
    Pocion();
    Pocion(int bx, int by, int tipoPocion, float x, float y, sf::Texture *tex);
    virtual ~Pocion();
    sf::Sprite getSprite();
    void destroy();
    void render(float sec);
    int getTipo(); // 1: vida, 2: velocidad, 3: fuerza
private:
    int tipo;
    float times4;
    int sentidoPocion;
    int contPocion;
};

#endif /* POCION_H */

