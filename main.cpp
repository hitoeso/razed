/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Miguel y Luis
 *
 * Created on 23 de febrero de 2016, 9:36
 */

#include <iostream>
#include <typeinfo>
#include <string>
#include <math.h>
#include <cmath>
#include <stdlib.h>
#include "vector"
#include <cstddef>

#include "Juego.h"

int main()
{        
    //Cargo la imagen donde reside la textura del sprite
    
   
    //tiempo
    Juego *juego = Juego::Instance();
    juego->inicializarJuego();

    while (juego->getWindow()->isOpen())
    {
        juego->run();
    }

    return 0;
}

