/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   bala.cpp
 * Author: Miguel y Luis
 * 
 * Created on 14 de marzo de 2016, 18:03
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bala.cpp
 * Author: Raquel
 * 
 * Created on 10 de marzo de 2016, 15:04
 */
#include "Bala.h"
#include <math.h>

#define PI 3.14159265


Bala::Bala() {
}

Bala::Bala(int bx, int by, float x, float y, float velocidad, float life, float fuerza, double mouseX, double mouseY, sf::Texture *tex, int type) : Colisionable(bx, by) {
    
    //Calculamos los vectores de dirección de x e y
    sprite = new sf::Sprite(*tex);

    float angx;
    float angy;  
    sprite->setOrigin(32/2, 32/2);
    sprite->setTextureRect(sf::IntRect(32, 32*11, 32, 32));
    sprite->setPosition(x, y);
    if(type==0){
        angx = mouseX-x;
        angy = mouseY-y;
    }
    float mod = sqrt(angy*angy+angx*angx);
    dirx = angx/mod;
    diry = angy/mod;
    sprite->setRotation(atan2(angy, angx) * 180 / PI);
       
    inicialX = x;
    inicialY = y;
    posX = x;
    posY = y;
    vel = velocidad;
    vida = life;
    time = 0;   
    clock2 = new sf::Clock();
    danio = fuerza;
}

bool Bala::update(sf::Time *deltaTime) {
    if(time < vida){
        elapsed = new sf::Time(clock2->getElapsedTime());
        time = elapsed->asSeconds();
        delete elapsed;
        posX = inicialX+(vel*time)*dirx;
        posY = inicialY+(vel*time)*diry;
        return true;
    }
    else{
        
        this->destroy();
        return false;
    }
}

void Bala::render(){
    sprite->setPosition(posX,posY);
}

float Bala::getDanio() {
    return danio;
}

sf::Sprite Bala::getSprite(){
    return *sprite;
}

void Bala::destroy() {
    delete clock2;
    delete sprite;
    delete this;
}

Bala::~Bala() {
}


