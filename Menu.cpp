/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Menu.cpp
 * Author: Raquel
 * 
 * Created on 18 de abril de 2016, 14:48
 */

#include "Menu.h"

Menu* Menu::pinstance = 0;

Menu* Menu::Instance(){
    if(pinstance == 0){
        pinstance = new Menu();
    }
    return pinstance;
}


Menu::Menu() {
    juego = Juego::Instance();
    tMenu = new sf::Texture;
    tSelec = new sf::Texture;
    tFondo0 = new sf::Texture;
    tFondo1 = new sf::Texture;
    tFondo2 = new sf::Texture;
    tFondo3 = new sf::Texture;
    sonido = juego->getSonido();
    musica = juego->getMusica();
    if (!tMenu->loadFromFile("resources/MainButtons.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    if (!tSelec->loadFromFile("resources/seleccionsheet.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    if (!tFondo0->loadFromFile("resources/Background.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    if (!tFondo1->loadFromFile("resources/pause.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    if (!tFondo2->loadFromFile("resources/opciones.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    if (!tFondo3->loadFromFile("resources/seleccion.png"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
}

void Menu::setStatus(int i, float sx, float sy) {    
    type=i;
    if(type==0){
        juego->cambiarMusica("intro.ogg");
        botones.clear();
        texto.clear();
        barras.clear();
        tarjeta.clear();
        personajes.clear();
        armas.clear();

        fondo = new sf::Sprite(*tFondo0);
        fondo->setOrigin(800/2,480/2);
        fondo->setTextureRect(sf::IntRect(0, 0, 800, 480));
        fondo->setPosition(800*sx/2, 480*sy/2);
        fondo->scale(sx, sy);
        for(int j=0; j<4; j++){
            botones.push_back(new sf::Sprite(*tMenu));
            botones[j]->setOrigin((8.5*32)/2,(1.66*32)/2);
            botones[j]->setTextureRect(sf::IntRect(0, j*2*32, 8.5*32, 1.66*32));
            botones[j]->setPosition(800*sx/2, (4+j*1.25)*55*sy);
            botones[j]->scale(sx, sy);
        }
    }
    if(type==1){
        texto.clear();
        barras.clear();
        botones.clear();
        tarjeta.clear();
        personajes.clear();
        armas.clear();
        
        fondo = new sf::Sprite(*tFondo1);
        fondo->setOrigin(800/2,480/2);
        fondo->setTextureRect(sf::IntRect(0, 0, 800, 480));
        fondo->setPosition(800*sx/2, 480*sy/2);
        fondo->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));
        botones[0]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[0]->setTextureRect(sf::IntRect(0, 10*32, 8.5*32, 1.66*32));
        botones[0]->setPosition(800*sx/2, 4*55*sy);
        botones[0]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));
        botones[1]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[1]->setTextureRect(sf::IntRect(0, 6*32, 8.5*32, 1.66*32));
        botones[1]->setPosition(800*sx/2, 5.25*55*sy);
        botones[1]->scale(sx, sy);
    }
    if(type==2){
        botones.clear();
        tarjeta.clear();
        personajes.clear();
        armas.clear();
        
        fondo = new sf::Sprite(*tFondo2);
        fondo->setOrigin(800/2,480/2);
        fondo->setTextureRect(sf::IntRect(0, 0, 800, 480));
        fondo->setPosition(800*sx/2, 480*sy/2);
        fondo->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));
        botones[0]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[0]->setTextureRect(sf::IntRect(0, 12*32, 8.5*32, 1.66*32));
        botones[0]->setPosition(800*sx/2, 3.2*55*sy);
        botones[0]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));
        botones[1]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[1]->setTextureRect(sf::IntRect(0, 16*32, 8.5*32, 1.66*32));
        botones[1]->setPosition(800*sx/2, 4.2*55*sy);
        botones[1]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));
        botones[2]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[2]->setTextureRect(sf::IntRect(0, 20*32, 8.5*32, 1.66*32));
        botones[2]->setPosition(800*sx/2, 8.05*55*sy);
        botones[2]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));//boton menos 1
        botones[3]->setOrigin((1*32)/2,(1*32)/2);
        botones[3]->setTextureRect(sf::IntRect(31*32, 22*32, 1*32, 1*32));
        botones[3]->setPosition(600*sx/2, 5.61*55*sy);
        botones[3]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));//boton mas 1
        botones[4]->setOrigin((1*32)/2,(1*32)/2);
        botones[4]->setTextureRect(sf::IntRect(30*32, 22*32, 1*32, 1*32));
        botones[4]->setPosition(1000*sx/2, 5.62*55*sy);
        botones[4]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));//boton menos 2
        botones[5]->setOrigin((1*32)/2,(1*32)/2);
        botones[5]->setTextureRect(sf::IntRect(31*32, 22*32, 1*32, 1*32));
        botones[5]->setPosition(600*sx/2, 7.01*55*sy);
        botones[5]->scale(sx, sy);
        
        botones.push_back(new sf::Sprite(*tMenu));//boton mas 2
        botones[6]->setOrigin((1*32)/2,(1*32)/2);
        botones[6]->setTextureRect(sf::IntRect(30*32, 22*32, 1*32, 1*32));
        botones[6]->setPosition(1000*sx/2, 7.02*55*sy);
        botones[6]->scale(sx, sy);
        
        barras.push_back(new sf::Sprite(*tMenu));//barra 1
        barras[0]->setOrigin((5*32)/2,(1.15*32)/2);
        barras[0]->setTextureRect(sf::IntRect(25*32, 22*32, 5*32, 1.15*32));
        barras[0]->setPosition(800*sx/2, 5.65*55*sy);
        barras[0]->scale(sx, sy);
        
        barras.push_back(new sf::Sprite(*tMenu));//barra 2
        barras[1]->setOrigin((5*32)/2,(1.15*32)/2);
        barras[1]->setTextureRect(sf::IntRect(25*32, 22*32, 5*32, 1.15*32));
        barras[1]->setPosition(800*sx/2, 7.05*55*sy);
        barras[1]->scale(sx, sy);
        
        texto.push_back(new sf::Sprite(*tMenu));
        texto[0]->setOrigin((5*32)/2,(1*32)/2);
        texto[0]->setTextureRect(sf::IntRect(0, 24*32, 5*32, 1*32));
        texto[0]->setPosition(810*sx/2, 5.1*55*sy);
        texto[0]->scale(sx, sy);

        texto.push_back(new sf::Sprite(*tMenu));
        texto[1]->setOrigin((6*32)/2,(1*32)/2);
        texto[1]->setTextureRect(sf::IntRect(5*32, 24*32, 6*32, 1*32));
        texto[1]->setPosition(822.5*sx/2, 6.5*55*sy);
        texto[1]->scale(sx, sy);
    }
    if(type==3){
        botones.clear();
        texto.clear();
        barras.clear();
        tarjeta.clear();
        personajes.clear();
        armas.clear();
        
        clock = new sf::Clock();
        animPeque = 0;
        sentidoPeque= 1;
        
        fondo = new sf::Sprite(*tFondo3);
        fondo->setOrigin(800/2,480/2);
        fondo->setTextureRect(sf::IntRect(0, 0, 800, 480));
        fondo->setPosition(800*sx/2, 480*sy/2);
        fondo->scale(sx, sy);
        
        personajes.push_back(new sf::Sprite(*tSelec));
        personajes[0]->setOrigin((5*32)/2, (5*32)/2);
        personajes[0]->setTextureRect(sf::IntRect(0*32, 0*32, 5*32, 5*32));
        personajes[0]->setPosition(32*3*sx, 400*sy);
        personajes[0]->scale(0.4*sx, 0.4*sy);
        
        personajes.push_back(new sf::Sprite(*tSelec));
        personajes[1]->setOrigin((5*32)/2, (5*32)/2);
        personajes[1]->setTextureRect(sf::IntRect(0*32, 5*32, 5*32, 5*32));
        personajes[1]->setPosition(32*6*sx, 400*sy);
        personajes[1]->scale(0.4*sx, 0.4*sy);
        
        personajes.push_back(new sf::Sprite(*tSelec));
        personajes[2]->setOrigin((5*32)/2, (5*32)/2);
        personajes[2]->setTextureRect(sf::IntRect(24*32, 5*32, 5*32, 5*32));
        personajes[2]->setPosition(32*9*sx, 400*sy);
        personajes[2]->scale(0.4*sx, 0.4*sy);
        
        personajes.push_back(new sf::Sprite(*tSelec));
        personajes[3]->setOrigin((5*32)/2, (5*32)/2);
        personajes[3]->setTextureRect(sf::IntRect(24*32, 5*32, 5*32, 5*32));
        personajes[3]->setPosition(32*12*sx, 400*sy);
        personajes[3]->scale(0.4*sx, 0.4*sy);
        
        //Cuadro de selección
        /*
        personajes.push_back(new sf::Sprite(*tSelec));
        personajes[4]->setOrigin((6*32)/2, (6*32)/2);
        personajes[4]->setTextureRect(sf::IntRect(24*32, 22*32, 6*32, 6*32));
        personajes[4]->setPosition(96.5*sx, 399*sy);
        personajes[4]->scale(0.45*sx, 0.45*sy);
        */
        armas.push_back(new sf::Sprite(*tSelec));
        armas[0]->setOrigin((5*32)/2, (2*32)/2);
        armas[0]->setTextureRect(sf::IntRect(0*32, 27*32, 5*32, 2*32));
        armas[0]->setPosition(32*20*sx, 170*sy);
        armas[0]->scale(0.7*sx, 0.7*sy);
        
        armas.push_back(new sf::Sprite(*tSelec));
        armas[1]->setOrigin((5*32)/2, (2*32)/2);
        armas[1]->setTextureRect(sf::IntRect(6*32, 29*32, 5*32, 2*32));
        armas[1]->setPosition(32*20*sx, 250*sy);
        armas[1]->scale(0.7*sx, 0.7*sy);
        
        //Cuadro de selección
        armas.push_back(new sf::Sprite(*tSelec));
        armas[2]->setOrigin((6*32)/2, (3*32)/2);
        armas[2]->setTextureRect(sf::IntRect(24*32, 28*32, 6*32, 6*32));
        armas[2]->setPosition(32*20*sx, 170*sy);
        armas[2]->scale(0.7*sx, 0.7*sy);
        
        tarjeta.push_back(new sf::Sprite(*tSelec));
        tarjeta[0]->setOrigin((5*32)/2, (5*32)/2);
        tarjeta[0]->setTextureRect(sf::IntRect(0*32, 0*32, 5*32, 5*32));
        tarjeta[0]->setPosition(45*3*sx, 170*sy);
        tarjeta[0]->scale(0.7*sx, 0.7*sy);
        
        tarjeta.push_back(new sf::Sprite(*tSelec));
        tarjeta[1]->setOrigin((10*32)/2, (7*32)/2);
        tarjeta[1]->setTextureRect(sf::IntRect(0*32, 10*32, 10*32, 7*32));
        tarjeta[1]->setPosition(106*3*sx, 180*sy);
        tarjeta[1]->scale(0.6*sx, 0.6*sy);
        
        tarjeta.push_back(new sf::Sprite(*tSelec));
        tarjeta[2]->setOrigin((16*32)/2, (3*32)/2);
        tarjeta[2]->setTextureRect(sf::IntRect(0*32, 17*32, 16*32, 3*32));
        tarjeta[2]->setPosition(76*3*sx, 275*sy);
        tarjeta[2]->scale(0.6*sx, 0.6*sy);

        botones.push_back(new sf::Sprite(*tMenu));
        botones[0]->setOrigin((8.5*32)/2,(1.66*32)/2);
        botones[0]->setTextureRect(sf::IntRect(0, 0, 8.5*32, 1.66*32));
        botones[0]->setPosition(640*sx, 405*sy);
        botones[0]->scale(sx, sy);
    }
}
void Menu::leaving(){
    
}

void Menu::updatePersonaje(int pers, int numeroDeFrames, int sx, int sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if((x > personajes[pers]->getPosition().x-(5*32*sx/2) && x < personajes[pers]->getPosition().x+((5*32)*sx/2))){
        if (y > personajes[pers]->getPosition().y-((5*32)*sy/2) && y < personajes[pers]->getPosition().y+((5*32)*sy/2)){
            elapsed = new sf::Time(clock->getElapsedTime());
            float times2 = elapsed->asSeconds();
            if(times2-times>0.15){
                 //sprite->setPosition(posXC, posYC);
                 personajes[pers]->setTextureRect(sf::IntRect((animPeque)*6*32, pers*5*32, 32*5, 32*5));
                 if(sentidoPeque==1)animPeque++;
                 if(sentidoPeque==-1)animPeque--;
                 if((animPeque) == numeroDeFrames || (animPeque) == 0) sentidoPeque=sentidoPeque*-1;

                 times=times2;  
            }
        }
        else{
            personajes[pers]->setTextureRect(sf::IntRect(0*32, pers*5*32, 5*32, 5*32));
            /*animPeque=0;
            sentidoPeque=1;*/
        }
    }
    else{
        personajes[pers]->setTextureRect(sf::IntRect(0*32, pers*5*32, 5*32, 5*32));
        /*animPeque=0;
        sentidoPeque=1;*/
    }
}

void Menu::update(float sx, float sy){
    if(type == 0){
        for (int j=0; j<botones.size(); j++){
            updateBoton(botones[j], j*2, sx, sy);
        }
    }
    else if(type == 1){
        updateBoton(botones[0], 10, sx, sy);
        updateBoton(botones[1], 6, sx, sy);
    }
    else if(type == 2){
        if(musica)
            updateBoton(botones[0], 12, sx, sy);
        else
            updateBoton(botones[0], 14, sx, sy);
        if(sonido)
            updateBoton(botones[1], 16, sx, sy);
        else
            updateBoton(botones[1], 18, sx, sy);
        updateBoton(botones[2], 20, sx, sy);
        updateBoton2(botones[3], 31, sx, sy);
        updateBoton2(botones[4], 30, sx, sy);
        updateBoton2(botones[5], 31, sx, sy);
        updateBoton2(botones[6], 30, sx, sy);
    }
    else if(type==3){
        updateBoton(botones[0], 0, sx, sy);
        updatePersonaje(0, 2, sx, sy);
        updatePersonaje(1, 2, sx, sy);
    }
    
}
void Menu::draw(){
    juego->getWindow()->draw(*fondo);
    for(int i=0; i<botones.size(); i++){
        juego->getWindow()->draw(*botones[i]);
    }
    for(int j=0; j<barras.size(); j++){
        juego->getWindow()->draw(*barras[j]);
    }
    for(int z=0; z<texto.size(); z++){
        juego->getWindow()->draw(*texto[z]);
    }
    for(int w=0; w<personajes.size(); w++){
        juego->getWindow()->draw(*personajes[w]);
    }
    for(int y=0; y<armas.size(); y++){
        juego->getWindow()->draw(*armas[y]);
    }
    for(int x=0; x<tarjeta.size(); x++){
        juego->getWindow()->draw(*tarjeta[x]);
    }
}
void Menu::updateBoton(sf::Sprite *spr, int num, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if((x > spr->getPosition().x-((8.5*32)*sx/2) && x < spr->getPosition().x+((8.5*32)*sx/2))){
       if (y > spr->getPosition().y-((1.66*32)*sy/2) && y < spr->getPosition().y+((1.66*32)*sy/2)){
           spr->setTextureRect(sf::IntRect(9*32, num*32, 8.5*32, 1.66*32));
           spr->setScale(1.1*sx,1.1*sy);
       }
       else{
           spr->setTextureRect(sf::IntRect(0, num*32, 8.5*32, 1.66*32));
           spr->setScale(1*sx,1*sy);
       }
   }
    else{
        spr->setTextureRect(sf::IntRect(0, num*32, 8.5*32, 1.66*32));
        spr->setScale(1*sx,1*sy);
    }
}

void Menu::updateTarjeta(int num, int sx, int sy){

    tarjeta[0]->setTextureRect(sf::IntRect(0*32, num*5*32, 5*32, 5*32));
    tarjeta[1]->setTextureRect(sf::IntRect(num*10*32, 10*32, 10*32, 7*32));
    tarjeta[2]->setTextureRect(sf::IntRect(num*16*32, 17*32, 16*32, 3*32));
    
    /*
    delete personajes[4];
    personajes.erase(personajes.begin()+4);
    personajes[personajes.size()]=NULL;
    
    personajes.push_back(new sf::Sprite(*tSelec));
    personajes[4]->setOrigin((6*32)/2, (6*32)/2);
    personajes[4]->setTextureRect(sf::IntRect(24*32, 22*32, 6*32, 6*32));
    personajes[4]->setPosition(32*5.14*(num+1)*sx, 640*sy);
    std::cout<<32*5.14*(num+1)*sx<<" aqui"<<std::endl;
    std::cout<<640*sy<<" aqui2"<<std::endl;
    //personajes[4]->setPosition(300, 900);
    personajes[4]->scale(0.75*sx, 0.75*sy);
     */
            
}

void Menu::click(float sx, float sy){
   double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY(); 
    if(type==0){ 
        if((x > botones[0]->getPosition().x-((8.5*32)*sx/2) && x < botones[0]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[0]->getPosition().y-((1.66*32)*sy/2) && y < botones[0]->getPosition().y+((1.66*32)*sy/2)){
                juego->playSonido();
                //juego->setState(2, -1);
                this->setStatus(3, sx, sy);
                return;
           }
        }
        if((x > botones[1]->getPosition().x-((8.5*32)*sx/2) && x < botones[1]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[1]->getPosition().y-((1.66*32)*sy/2) && y < botones[1]->getPosition().y+((1.66*32)*sy/2)){
                juego->playSonido();
                this->setStatus(2, sx, sy);
                return;
           }
        }
        if((x > botones[3]->getPosition().x-((8.5*32)*sx/2) && x < botones[3]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[3]->getPosition().y-((1.66*32)*sy/2) && y < botones[3]->getPosition().y+((1.66*32)*sy/2)){
               juego->getWindow()->close();
           }
        }
    }
    if(type==1){ 
        if((x > botones[0]->getPosition().x-((8.5*32)*sx/2) && x < botones[0]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[0]->getPosition().y-((1.66*32)*sy/2) && y < botones[0]->getPosition().y+((1.66*32)*sy/2)){
                juego->playSonido();
                juego->setState(2, -1);
                return;
           }
        }
        if((x > botones[1]->getPosition().x-((8.5*32)*sx/2) && x < botones[1]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[1]->getPosition().y-((1.66*32)*sy/2) && y < botones[1]->getPosition().y+((1.66*32)*sy/2)){
               juego->setState(1, 0);
               //destruir el ingame para que cuando le des a jugar again no te vuelva a meter a la misma partida sino que empiece
               //otra partida desde cero
               //ademas, hay que ver si desde el de pausa le das a opciones, luego a atras, que te lleve al de pausa otra vez
           }
        }
    }
    else if(type==2){
        if((x > botones[0]->getPosition().x-((8.5*32)*sx/2) && x < botones[0]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[0]->getPosition().y-((1.66*32)*sy/2) && y < botones[0]->getPosition().y+((1.66*32)*sy/2)){
               this->updateMusica(botones[0], sx, sy);
           }
        }
        if((x > botones[1]->getPosition().x-((8.5*32)*sx/2) && x < botones[1]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[1]->getPosition().y-((1.66*32)*sy/2) && y < botones[1]->getPosition().y+((1.66*32)*sy/2)){
               this->updateSonido(botones[1], sx, sy);
           }
        }
        if((x > botones[2]->getPosition().x-((8.5*32)*sx/2) && x < botones[2]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[2]->getPosition().y-((1.66*32)*sy/2) && y < botones[2]->getPosition().y+((1.66*32)*sy/2)){
                juego->playSonido();
                this->setStatus(0, sx, sy);
           }
        }
        if((x > botones[3]->getPosition().x-((8.5*32)*sx/2) && x < botones[3]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[3]->getPosition().y-((1.66*32)*sy/2) && y < botones[3]->getPosition().y+((1.66*32)*sy/2)){
                this->cambiarVolumenMusica(botones[4], botones[3], barras[0], sx, sy);
           }
        }
        if((x > botones[4]->getPosition().x-((8.5*32)*sx/2) && x < botones[4]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[4]->getPosition().y-((1.66*32)*sy/2) && y < botones[4]->getPosition().y+((1.66*32)*sy/2)){
                this->cambiarVolumenMusica(botones[4], botones[3], barras[0], sx, sy);
           }
        }
        if((x > botones[5]->getPosition().x-((8.5*32)*sx/2) && x < botones[5]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[5]->getPosition().y-((1.66*32)*sy/2) && y < botones[5]->getPosition().y+((1.66*32)*sy/2)){
                this->cambiarVolumenEfectos(botones[6], botones[5], barras[1], sx, sy);
                juego->playSonido();
           }
        }
        if((x > botones[6]->getPosition().x-((8.5*32)*sx/2) && x < botones[6]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[6]->getPosition().y-((1.66*32)*sy/2) && y < botones[6]->getPosition().y+((1.66*32)*sy/2)){
                this->cambiarVolumenEfectos(botones[6], botones[5], barras[1], sx, sy);
                juego->playSonido();
           }
        }
    }
    else if(type==3){
        if((x > botones[0]->getPosition().x-((8.5*32)*sx/2) && x < botones[0]->getPosition().x+((8.5*32)*sx/2))){
           if (y > botones[0]->getPosition().y-((1.66*32)*sy/2) && y < botones[0]->getPosition().y+((1.66*32)*sy/2)){
                juego->playSonido();
                juego->setState(2, -1);
                return;
           }
        }
        if((x > personajes[0]->getPosition().x-(5*32*sx/2) && x < personajes[0]->getPosition().x+((5*32)*sx/2))){
            if (y > personajes[0]->getPosition().y-((5*32)*sy/2) && y < personajes[0]->getPosition().y+((5*32)*sy/2)){
                juego->setPersonaje(0);
                juego->playSonido();
                updateTarjeta(0, sx, sy);
            }
        }
        if((x > personajes[1]->getPosition().x-(5*32*sx/2) && x < personajes[1]->getPosition().x+((5*32)*sx/2))){
            if (y > personajes[1]->getPosition().y-((5*32)*sy/2) && y < personajes[1]->getPosition().y+((5*32)*sy/2)){
                juego->setPersonaje(1);
                juego->playSonido();
               updateTarjeta(1, sx, sy);
            }
        }
    }
}
void Menu::updateBoton2(sf::Sprite *spr, int num, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if(x > spr->getPosition().x-((1*32)*sx/2) && x < spr->getPosition().x+((1*32)*sx/2)){
       if (y > spr->getPosition().y-((1*32)*sy/2) && y < spr->getPosition().y+((1*32)*sy/2)){
           spr->setTextureRect(sf::IntRect((num+2)*32, 22*32, 1*32, 1*32));
           spr->setScale(1.1*sx,1.1*sy);
       }
       else{
           spr->setTextureRect(sf::IntRect(num*32, 22*32, 1*32, 1*32));
           spr->setScale(1*sx,1*sy);
       }
   }
    else{
        spr->setTextureRect(sf::IntRect(num*32, 22*32, 1*32, 1*32));
        spr->setScale(1*sx,1*sy);
    }
}

void Menu::updateSonido(sf::Sprite *spr, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if(x > spr->getPosition().x-((8.5*32)*sx/2) && x < spr->getPosition().x+((8.5*32)*sx/2)){
       if (y > spr->getPosition().y-((1.66*32)*sy/2) && y < spr->getPosition().y+((1.66*32)*sy/2)){
           juego->setSonido();
           sonido = juego->getSonido();
           if(!sonido){
                spr->setTextureRect(sf::IntRect(0,8*2*32, 8.5*32, 1.66*32));
           }
           else{
                spr->setTextureRect(sf::IntRect(0,9*2*32, 8.5*32, 1.66*32));
           }
           
       }
    }
}

void Menu::updateMusica(sf::Sprite *spr, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if(x > spr->getPosition().x-((8.5*32)*sx/2) && x < spr->getPosition().x+((8.5*32)*sx/2)){
       if (y > spr->getPosition().y-((1.66*32)*sy/2) && y < spr->getPosition().y+((1.66*32)*sy/2)){
            juego->setMusica(); 
            musica = juego->getMusica();
            if(!musica){
                spr->setTextureRect(sf::IntRect(0, 6*2*32, 8.5*32, 1.66*32));
            }
            else{
                spr->setTextureRect(sf::IntRect(0, 7*2*32, 8.5*32, 1.66*32));
            }
       }
    }
}

void Menu::cambiarVolumenMusica(sf::Sprite *spr, sf::Sprite *spr2, sf::Sprite *spr3, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();
    if(x > spr->getPosition().x-((1*32)*sx/2) && x < spr->getPosition().x+((1*32)*sx/2)){
        if (y > spr->getPosition().y-((1*32)*sy/2) && y < spr->getPosition().y+((1*32)*sy/2)){
            if(juego->getVolMusica()<80 && juego->getVolMusica()>=0){
               juego->setVolMusica(juego->getVolMusica()+20);
               spr3->setTextureRect(sf::IntRect((juego->getVolMusica()/20)*5*32,22*32, 5*32, 1.15*32));
            }
            else if(juego->getVolMusica()>=80){
               juego->setVolMusica(100);
               spr3->setTextureRect(sf::IntRect(5*5*32,22*32, 5*32, 1.15*32));
            }
        }
    }
    if(x > spr2->getPosition().x-((1*32)*sx/2) && x < spr2->getPosition().x+((1*32)*sx/2)){
       if (y > spr2->getPosition().y-((1*32)*sy/2) && y < spr2->getPosition().y+((1*32)*sy/2)){
            if(juego->getVolMusica()>20 && juego->getVolMusica()<=100){
               juego->setVolMusica(juego->getVolMusica()-20);
               spr3->setTextureRect(sf::IntRect((juego->getVolMusica()/20)*5*32,22*32, 5*32, 1.15*32));
            }
            else if(juego->getVolMusica()<=20){
                juego->setVolMusica(0);
                spr3->setTextureRect(sf::IntRect(0,22*32, 5*32, 1.15*32));
            }
        }
    } 
}

void Menu::cambiarVolumenEfectos(sf::Sprite *spr, sf::Sprite *spr2, sf::Sprite *spr3, float sx, float sy){
    double x;
    double y;
    x = juego->getMouseX();
    y = juego->getMouseY();    
    if(x > spr->getPosition().x-((1*32)*sx/2) && x < spr->getPosition().x+((1*32)*sx/2)){
       if (y > spr->getPosition().y-((1*32)*sy/2) && y < spr->getPosition().y+((1*32)*sy/2)){
           if(juego->getVolSonido()<80 && juego->getVolSonido()>=0){
               juego->setVolSonido(juego->getVolSonido()+20);
               spr3->setTextureRect(sf::IntRect((juego->getVolSonido()/20)*5*32,22*32, 5*32, 1.15*32));
           }
           else if(juego->getVolSonido()>=80){
               juego->setVolSonido(100);
               spr3->setTextureRect(sf::IntRect(5*5*32,22*32, 5*32, 1.15*32));
           }
       }
    }
    if(x > spr2->getPosition().x-((1*32)*sx/2) && x < spr2->getPosition().x+((1*32)*sx/2)){
       if (y > spr2->getPosition().y-((1*32)*sy/2) && y < spr2->getPosition().y+((1*32)*sy/2)){
           if(juego->getVolSonido()>20 && juego->getVolSonido()<=100){
               juego->setVolSonido(juego->getVolSonido()-20);
               spr3->setTextureRect(sf::IntRect((juego->getVolSonido()/20)*5*32,22*32, 5*32, 1.15*32));
           }
           else if(juego->getVolSonido()<=20){
                juego->setVolSonido(0);
                spr3->setTextureRect(sf::IntRect(0,22*32, 5*32, 1.15*32));
            }
       }
   }
}
void Menu::destroy(){
    delete this;
}
Menu::~Menu() {
    
}
