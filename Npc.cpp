/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Npc.cpp
 * Author: Raquel
 * 
 * Created on 1 de marzo de 2016, 10:08
 */
using namespace std;
#include "Npc.h"
#include <iostream>
#include <SFML/Graphics.hpp>

#define PI 3.141592

Npc::Npc() {
}

Npc::Npc(int bx, int by, float x,float y, sf::Texture *tex, int v, int tipoNPC) : Colisionable(bx, by) {
    juego = Juego::Instance();

    
    //0 normal, 1 rapido, 2 gordo, 3 boss
    
    contAnim=5;
    
    vida = v*2;
    anim = 3;
    velocidad = 1;
    cadaver = 1;
    contAnim = 5;
    bossVel = 0.05;
    secondsBoss = 0;
    pos1 = 0;
    pos2 = 0;
    contLin = 0;
    times2 = 0;
    clock = new sf::Clock;
    clockBoss = new sf::Clock;
    time = new sf::Time(clock->getElapsedTime());
    golpe = false;
    aturdido = false;
    estado = true;
    reloj = new sf::Clock;
    tipo =tipoNPC;
	timeExpl=time->asSeconds();
	   vuelta=true;
    time= new sf::Time();
    clock= new sf::Clock();
	
	
	
    if(tipo==4){
        contLin=64;
        vida=300;
        sprite = new sf::Sprite(*tex);
        sprite->setOrigin(64/2, 64/2);
        sprite->setTextureRect(sf::IntRect(32, 32*3, 32, 32));
        sprite->setPosition(x, y);
        sentido=1;
        contAnim=5;
        timeNpc=0;
        velocidad=7;
    }
    else{
        if(tipo==1){
            vida=vida*0.5;
            velocidad=1.5;
            cadaver=5;
            anim=5;
        }
        else if(tipo==2){
            vida=vida*2;
            velocidad=0.8;
            anim=4;
            cadaver=3;
        }
        sprite = new sf::Sprite(*tex);
        sprite->setOrigin(32/2, 32/2);
        sprite->setTextureRect(sf::IntRect(32, 32*3, 32, 32));
        sprite->setPosition(x, y);
        sentido=1;
        contAnim=1;
        timeNpc=0;
    }
}

std::vector<float> Npc::calcularNPC(std::vector<Colisionable*>* c){
    std::vector<float> vectorNPC;
    vectorNPC.push_back(0);
    vectorNPC.push_back(0);
    float d, posx, posy;
    for(int z = 0; z<c->size(); z++){
        if(c->at(z)!=NULL){
            if(Npc *npc = dynamic_cast<Npc*> (c->at(z))){
                posx= sprite->getPosition().x - c->at(z)->getSprite()->getPosition().x;
                posy = sprite->getPosition().y - c->at(z)->getSprite()->getPosition().y;
                d = sqrt(pow(posx, 2) + pow(posy, 2));                
                if(d<=35){
                    vectorNPC[0] += posx;
                    vectorNPC[1] += posy;
                }
            }
        }
    }

    float mod = sqrt(pow(vectorNPC[0], 2) + pow(vectorNPC[1], 2));
    if(mod != 0){
        vectorNPC[0] = vectorNPC[0]/mod;
        vectorNPC[1] = vectorNPC[1]/mod;
    }
    return vectorNPC;
}

void Npc::perseguir(float posPX, float posPY, sf::Time* deltaTime, std::vector<Colisionable*>* c, std::vector<Colisionable*>* p){
    
    if(secondsBoss<7 ){
        std::vector<float> vectorNPC = calcularNPC(c);
        std::vector<float> vectorPared = calcularPared(p);

        float posx= posPX-(sprite->getPosition().x);
        float posy= posPY-sprite->getPosition().y;
        float mod=sqrt((posy*posy)+(posx*posx));
        float aleatoriox = static_cast<float> (rand()) / static_cast<float> (RAND_MAX);
        float aleatorioy = static_cast<float> (rand()) / static_cast<float> (RAND_MAX);
        float pos1= 5*(posx/mod) + 3*(vectorNPC[0]) + 70*(vectorPared[0]) + 3.5*aleatoriox; //anyadir un vector aleatorio con un 20
        float pos2= 5*(posy/mod) + 3*(vectorNPC[1]) + 70*(vectorPared[1]) + 3.5*aleatorioy;

        float modulo = sqrt(pos1*pos1+pos2*pos2);
        pos1 = pos1/modulo;
        pos2 = pos2/modulo;

        float dx = 0.05*pos1+0.95*angx;
        float dy = 0.05*pos2+0.95*angy;

        angx = dx;
        angy = dy;
        float rotation = (atan2(dy, dx)) * 180 / PI;


        float futurax = (pos1*deltaTime->asSeconds()*3000);
        float futuray = (pos2*deltaTime->asSeconds()*3000);
        if(abs(previousx - (0.02*futurax+sprite->getPosition().x)) < 0.6 && abs(previousy - (sprite->getPosition().y+futuray*0.02)) < 0.6){
            contPos++;
            if(contPos < 20){
                sprite->setRotation(rotation);
                previousx = sprite->getPosition().x;
                previousy = sprite->getPosition().y;
            }

        }
        else{
            contPos=0;
            sprite->setRotation(rotation);
            previousx = sprite->getPosition().x;
            previousy = sprite->getPosition().y;
        }
        if(tipo!=4){
            sprite->move(0.02*futurax*velocidad, 0.02*futuray*velocidad);
        }else{
            float dx = sprite->getPosition().x -posPX;
            float dy = sprite->getPosition().y- posPY;
          if(vuelta)
               if(fabs(dx)<50 && fabs(dy)<50){
                    golpe=true;
                  
               }      
            if(!golpe)
                sprite->move(0.02*futurax*velocidad*bossVel, 0.02*futuray*velocidad*bossVel);
        }   
    }
}


std::vector<float>  Npc::calcularPared(std::vector<Colisionable*>* c){
    std::vector<float> vectorPared;
    vectorPared.push_back(0);
    vectorPared.push_back(0);
    float d, posx, posy;
    for(int z = 0; z<c->size(); z++){
        Pared *p = dynamic_cast<Pared*> (c->at(z));
        std::vector<float> puntos = p->calculaPuntos();
        for(int u=0; u<puntos.size(); u=u+2){
            posx= sprite->getPosition().x - puntos[u];
            posy = sprite->getPosition().y - puntos[u+1];
            d = sqrt(pow(posx, 2) + pow(posy, 2));
            if(d<=50){
                posx=posx/pow(d,2);
                posy=posy/pow(d,2);
 
                vectorPared[0] += posx;
                vectorPared[1] += posy;
                
            }
        }
    }

    return vectorPared;
}

bool Npc::update(sf::Time *deltaTime, float posPX, float posPY, std::vector<Colisionable*>* c, std::vector<Colisionable*>* p) {
    *time=clock->getElapsedTime();
    if(estado){
        if((vida<1)){
            reloj->restart();
            juego->setPuntuacion(100);
            estado=false;
        }
        else{
            perseguir(posPX, posPY, deltaTime, c, p);
            //sprite->setPosition(sprite->getPosition().x+(0.009*deltaTime.asSeconds()*3000),sprite->getPosition().y);
            return true;
        }
    }else{
        if(tipo!=4){
            if(reloj->getElapsedTime().asSeconds()>10){
                this->destroy();
                return false;
            }
            return true;
        }else{
            
            
             if(numframe<8){
                if(time->asSeconds()-timeExpl>=0.1){
                  
                    sprite->setOrigin(64/2, 64/2);
                    sprite->setTextureRect(sf::IntRect((numframe*2)*32, 32*12, 64, 64));
                    sprite->setScale(4, 4);
                    numframe++;
                    timeExpl=time->asSeconds();
                }
         
                return true;
            }
            if(numframe>7){
     
           this->destroy();
            return false;
            }
           
        }
        
    }
}


void Npc::render(float tiempo){
    if(tipo!=4){
        if(estado){
            if(tiempo-timeNpc>=0.5){

                sprite->setTextureRect(sf::IntRect((contAnim)*32, 32*anim, 32, 32));
                if(sentido==1) contAnim++;
                else if(sentido==-1) contAnim--;
                if(contAnim == 7 || contAnim == 1) sentido=sentido*-1;
                timeNpc=tiempo;
            }
        }else{
             sprite->setTextureRect(sf::IntRect(32*cadaver, 32*7, 64, 32));
        }
    }else{
        animarBoss(tiempo);
    }
}

sf::Sprite* Npc::getSprite(){
    return sprite;
}

void Npc::perderVida(float fuerza){
    vida = vida-1*fuerza;
}

int Npc::getvida() {
    return vida;
}

void Npc::desplazar() {
    int desp = rand() %3 + (-1);
    int desp2 = rand() %3 + (-1);
    sprite->setPosition(Colisionable::getPosX()+desp,Colisionable::getPosY()+desp2);
}

Npc::~Npc() {

}

void Npc::destroy() {
    std::cout<<"mueronpc"<<std::endl;
    delete reloj;
    delete time;
    delete clock;
    delete clockBoss;
    delete sprite;
    delete this;
}


bool Npc::getEstado(){
    return(estado);
}

void Npc::animarBoss( float sec){
   
    if(estado){
        sprite->setScale(1.2,1.2);
       // sf::Time timeBoss = clockBoss.getElapsedTime();
        secondsBoss = clockBoss->getElapsedTime().asSeconds();




        if(!golpe){
            float vMov=0.4;
            if(bossVel>0.1){
                vMov=0.08;
            }else{
                vMov=0.4;
            }

            if(secondsBoss<7){

            if(sec-times2>vMov){
                
                 sprite->setPosition(sprite->getPosition().x, sprite->getPosition().y);
                 sprite->setTextureRect(sf::IntRect((contAnim)*64, contLin, 64, 64));
                 contAnim++;
                 if(contAnim==8){

                     contAnim=5;
                     if(contLin==64){
                     contLin=128;

                     }else{
                         contLin=64;

                     }
                 }
                 times2=sec;  
               }
            }else if(secondsBoss<10){

                 sprite->setTextureRect(sf::IntRect(4*64, 64, 64, 64));

            }else{
                bossVel=0.3;
                clockBoss->restart();
            }
        }else{

              sprite->setTextureRect(sf::IntRect(6*64, 196, 96, 54));
              bossVel=0;

        }

        if(bossVel>0.2 && clockBoss->getElapsedTime().asSeconds()>4){
            clockBoss->restart();
            bossVel=0.09;
        }

        if(golpe){
            if(clockBoss->getElapsedTime().asSeconds()>0.1){
                 vuelta=false;
                golpe=false;
               
                bossVel=0.09;
            }
 
        }
        if(!vuelta)
            if(clockBoss->getElapsedTime().asSeconds()>2){
             
                vuelta=true;
                clockBoss->restart();
            }
    }
}