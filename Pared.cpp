/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pared.cpp
 * Author: Carmina
 * 
 * Created on 20 de abril de 2016, 15:11
 */

#include "Pared.h"

Pared::Pared() {
}

Pared::Pared(float bx, float by, float x, float y, float w, float h, float rot) : Colisionable(bx, by){   
    sf::Texture *tex = new sf::Texture;

    if (!tex->loadFromFile("resources/yellow.gif"))
    {
        std::cerr << "Error cargando la imagen sprites.png";
        exit(0);
    }
    sprite = new sf::Sprite();
    sprite->setTextureRect(sf::IntRect(0, 0, w, h));
    if(rot!=0){
        sprite->setOrigin(0,0);
        sprite->rotate(rot);
        sprite->setPosition(x, y);
    }else{
        sprite->setOrigin(w/2, h/2);
        sprite->setPosition(x+w/2, y+h/2);       
    }
    
    px = x;
    py = y;
    width = w;
    height = h;
    
}

sf::Sprite* Pared::getSprite(){
    return sprite;
}

bool Pared::update(){
    return true;
}

Pared::~Pared() {
}

std::vector<float> Pared::calculaPuntos(){
    std::vector<float> puntos;
    
    float ancho = roundf(width/20);
    float alto = roundf(height/20);
    
    for(int i=0;i<=ancho;i++){
        puntos.push_back(px+i*width/ancho);
        puntos.push_back(py);
    }
    for(int j=1;j<=alto;j++){
        puntos.push_back(px);
        puntos.push_back(py+j*height/alto);
    }
    
    for(int k=1;k<=ancho;k++){
        puntos.push_back(px+k*width/ancho);
        puntos.push_back(py+height);
    }
    for(int l=1;l<alto;l++){
        puntos.push_back(px+width);
        puntos.push_back(py+l*height/alto);
    }
    
    return puntos;
}
float Pared::getPX(){
    return px;
}
float Pared::getPY(){
    return py;
}
float Pared::getWidth(){
    return width;
}
float Pared::getHeight(){
    return height;
}