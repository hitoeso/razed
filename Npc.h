/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Npc.h
 * Author: Raquel
 *
 * Created on 1 de marzo de 2016, 10:08
 */

#ifndef NPC_H
#define NPC_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <cmath>
#include <math.h>
#include "Colisionable.h"
#include "Pared.h"
#include "Juego.h"

class Npc : public Colisionable {
public:
    Npc();
    Npc(int bx, int by, float x, float y, sf::Texture *tex, int v, int tipo);
    bool update(sf::Time *deltaTime, float posPX, float posPY, std::vector<Colisionable*>* c, std::vector<Colisionable*>* p);
    void render(float);
    int getvida();
    void perderVida(float);
    sf::Sprite* getSprite();
    ~Npc();
    void destroy();
    void desplazar();
    bool getEstado();
    void animarBoss(float sec);
 
private:
    Juego *juego;
    
    float timeNpc;
    int sentido;
    int contAnim;
    int vida;
    float angx;
    float angy;
    float previousx;
    float previousy;
    int contPos;
    int contPos2;
    int anim;
    float velocidad;
    int cadaver;
    bool estado;
    sf::Clock *reloj;
    int tipo;
    void perseguir(float posPX, float posPY, sf::Time* deltaTime, std::vector<Colisionable*>* c, std::vector<Colisionable*>* p);
    std::vector<float> calcularPared(std::vector<Colisionable*>* c);
    std::vector<float> calcularNPC(std::vector<Colisionable*>* c);
    
    //BOSSS
    float bossVel;
    sf::Clock *clockBoss;  
    float pos1;
    float pos2;
    bool golpe;
    float posBossX;
    float posBossY;
    bool aturdido;
    int contLin;
    sf::Clock *clock;
    sf::Time *time;
 float times2;
  float secondsBoss;
  float timeExpl;
  int numframe;
  bool vuelta;
    
};

#endif /* NPC_H */