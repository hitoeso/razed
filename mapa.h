/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mapa.h
 * Author: Miguel y Luis
 *
 * Created on 19 de abril de 2016, 9:17
 */

#ifndef MAPA_H
#define MAPA_H
#include <SFML/Graphics.hpp>
#include "Colisionable.h"

class mapa {
public:
    mapa();
    mapa(const mapa& orig);
    virtual ~mapa();
    
    void leerMapa(std::vector<Colisionable*>* c);
    void dibuja(sf::RenderWindow *window);
    
    sf::Sprite fondo;
    int _width;
    int _tileWidth;
    int getWidth();
    int getHeight();
    
private:

    int ***_tilemap;
    int _numLayers;
    int _numObject;
    int _height;
    int _tileHeigth;
    
    int _widthCollider;
    int _heightCollider;
    int _xCollider;
    int _yCollider;
    int _rotCollider;
    
    sf::Sprite ****_tilemapSprite;
    sf::Sprite *_tilesetSprite;
    sf::Sprite te;
    
    sf::Texture _tilesetTexture;
    sf::Texture m_tileset;
    sf::Texture fond;
     
    sf::VertexArray m_vertices;
};

#endif /* MAPA_H */

