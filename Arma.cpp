/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arma.cpp
 * Author: Raquel
 * 
 * Created on 19 de abril de 2016, 19:22
 */

#include "Arma.h"

Arma::Arma(int tp, float f, float v, float cad, float tb, int carg, int tc) {
    juego = Juego::Instance();

    tipo = tp; 
    /* 1 - ARMA 1 JOHN BOY
     * 2 - ARMA 2 JOHN BOY
     * 3 - ARMA 1 ZACK TELLER
     * 4 - ARMA 2 ZACK TELLER 
    */
    fuerza = f;
    vel = v; 
    cadencia = cad; //velocidad normal
    timebala = tb;
    cargador = carg; //recarga media
    tCarga = tc;
    time = 0;
    numBalas = carg;
}

void Arma::setFuerza(float f){
    fuerza = f;
}

void Arma::update() {
    if(cargador == 0){
        elapsed = clock2.getElapsedTime();
        time = elapsed.asSeconds();        
        if(time>tCarga){
            setCargador(numBalas);
        }
    }   
}

int Arma::getTipo() {
    return tipo;
}


void Arma::setCadencia(float c){
    cadencia = c;
}

int Arma::getCargador() {
    return cargador;
}

void Arma::setCargador(int c) {
    cargador = cargador+c;
    if(cargador==0){
        clock2.restart();
        juego->sonidoRecarga();        
    }
}


void Arma::setTimeBala(float t){
    timebala = t;
}

void Arma::setVelocidadBala(float v){
    vel = v;
}

void Arma::setNumBalas(int n){
    numBalas = n;
}

float Arma::getFuerza(){
    return fuerza;
}

float Arma::getCadencia(){
    return cadencia;
}

float Arma::getTimeBala(){
    return timebala;
}

float Arma::getVelocidadBala(){
    return vel;
}

int Arma::getNumBalas(){
    return numBalas;
}

void Arma::destroy(){
    delete this;
}

Arma::~Arma() {
}