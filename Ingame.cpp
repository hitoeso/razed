/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Ingame.cpp
 * Author: Raquel
 * 
 * Created on 18 de abril de 2016, 14:48
 */

#include "Ingame.h"

Ingame* Ingame::pinstance = 0;

Ingame* Ingame::Instance(){
    if(pinstance == 0){
        pinstance = new Ingame;
    }
    return pinstance;
}

Ingame::Ingame() {
    cambioRondaClock = new sf::Clock();
    Mapa=new mapa();
    Mapa->leerMapa(&p);
    deltaClock = new sf::Clock();
    clock = new sf::Clock();
    clockRonda = new sf::Clock();
    iniciado = false;
    pausa = true;
}
void Ingame::setStatus(sf::Texture *tex, sf::Texture *hudbars, sf::Texture *hud, sf::Texture *baja, sf::Texture *dmg, sf::Font *fuentex) {    
    juego = Juego::Instance();
    
    std::cout << "pongo estado ingame" <<std::endl;
    gameover=false;
    clock->restart();
    cambioRondaClock->restart();
    deltaClock->restart();
    clockRonda->restart();
    iniciado = true;
    //Esto cuando esté el motor sería un string que tedría guardado la url de el sprite sheet general
    iniciado = true;
    textura = tex;
    texturahudbars = hudbars;
    texturahud = hud;
    bajavida = baja;
    texturadmg = dmg;
    juego->cambiarMusica("musica-ingame.ogg");
    pers = new Personaje(13, 13, 300,300, textura);
    pers->times2=0;
    view = new sf::View(sf::FloatRect(pers->getPosX(), pers->getPosY(), 600, 400));
    view2 = new sf::View(sf::FloatRect(10, 10, 600, 400));
    c.push_back(pers);
    totalZombis = 5;
    zombisGenerados = 0;
    dificultad = 3.0;
    nivel = 1;
    muertos = 0;
    
    sprite = new sf::Sprite(*hudbars);
    sprite->setOrigin(32/2, 32/2);
    sprite->setTextureRect(sf::IntRect(0*32, 10*32, 7*32, 32));
    sprite->setPosition(60, 40);
    sprite->setScale(0.8,0.8);
    
    sprite2 = new sf::Sprite(*hudbars);
    sprite2->setOrigin(32/2, 32/2);
    sprite2->setTextureRect(sf::IntRect(8*32, 10*32, 5*32, 32));
    sprite2->setPosition(50, 70);
    sprite2->setScale(0.8,0.8);
    
    sprite3 = new sf::Sprite(*hud);
    sprite3->setOrigin(32/2, 32/2);
    sprite3->setTextureRect(sf::IntRect(6*32, 2*32, 32, 32));
    sprite3->setPosition(160, 65);
    sprite3->setScale(0.8,0.8);
    
    pocion2 = new sf::Sprite(*hud);
    pocion2->setOrigin(1.5*32/2, 1.5*32/2);
    pocion2->setTextureRect(sf::IntRect(6*32-5, 4.47*32, 1.5*32, 1.5*32));
    pocion2->setPosition(240, 38);
    pocion2->setScale(0.5, 0.5);
    
    pocion3 = new sf::Sprite(*hud);
    pocion3->setOrigin(1.5*32/2, 1.5*32/2);
    pocion3->setTextureRect(sf::IntRect(6*32-5, 2.975*32, 1.5*32, 1.55*32));
    pocion3->setPosition(240, 37);
    pocion3->setScale(0.5,0.5);
    
    cara = new sf::Sprite(*hud);
    cara->setOrigin(32/2, 32/2);
    if(juego->getPersonaje()==0)
        cara->setTextureRect(sf::IntRect(3*32, 2*32, 2.75*32, 3*32));
    else if(juego->getPersonaje()==1)
        cara->setTextureRect(sf::IntRect(0*32, 3*32, 3*32, 3*32));
    cara->setPosition(10, 30);
    cara->setScale(0.7,0.7);
    
    bajahp = new sf::Sprite(*bajavida);
    bajahp->setOrigin(600/2, 400/2);
    bajahp->setTextureRect(sf::IntRect(0, 0, 600, 400));
    bajahp->setPosition(280, 200);
    
    extradmg = new sf::Sprite(*texturadmg);
    extradmg->setOrigin(96/2, 40/2);
    extradmg->setTextureRect(sf::IntRect(0, 0, 96, 37));
    extradmg->setPosition(195, 65);
    extradmg->setScale(0.45, 0.45);
    
    /*Contador balas y granadas*/
    balas = new sf::Sprite(*hud);
    balas->setOrigin(96/2, 40/2);
    balas->setTextureRect(sf::IntRect(0, 64, 32, 32));
    balas->setPosition(80, 93);
    balas->setScale(0.7, 0.7);
    
    granadas = new sf::Sprite(*hud);
    granadas->setOrigin(96/2, 40/2);
    granadas->setTextureRect(sf::IntRect(32, 64, 32, 32));
    granadas->setPosition(140, 93);
    granadas->setScale(0.7, 0.7);
    seconds=0;
    sprintf(string, "noche %d", nivel);
    fuente = new sf::Font();
    if(!fuente->loadFromFile("resources/upheavtt.ttf"))
        printf("Error cargando la fuente seleccionada");
    texto = new sf::Text();
    texto->setFont(*fuente);
    texto->setColor(sf::Color::White);
    texto->setCharacterSize(200);
    texto->setString(string);
    sf::FloatRect textRect = texto->getLocalBounds();
    texto->setOrigin(textRect.left + textRect.width/2, textRect.top  + textRect.height/2);
    texto->setPosition(280,140);
    noche = true;
    texto->setScale(0.2,0.2);
    
    sprintf(string2, "x%d",pers->getArma()->getCargador());
    texto2 = new sf::Text;
    texto2->setFont(*fuente);
    texto2->setColor(sf::Color::White);
    texto2->setCharacterSize(50);
    texto2->setString(string2);
    texto2->setScale(0.35,0.35);
    sf::FloatRect textRect2 = texto2->getLocalBounds();
    texto2->setOrigin(textRect2.left, textRect2.top  + textRect2.height/2);
    texto2->setPosition(70,90);
    
    sprintf(string3, "x%d", pers->getDisponibles());
    texto3 = new sf::Text;
    texto3->setFont(*fuente);
    texto3->setColor(sf::Color::White);
    texto3->setCharacterSize(50);
    texto3->setString(string3);
    texto3->setScale(0.35,0.35);
    sf::FloatRect textRect3 = texto3->getLocalBounds();
    texto3->setOrigin(textRect3.left, textRect3.top  + textRect3.height/2);
    texto3->setPosition(127,90);
    
    sprintf(string4, "+%d", juego->getPuntuacion());
    texto4 = new sf::Text;
    texto4->setFont(*fuente);
    texto4->setColor(sf::Color::White);
    texto4->setCharacterSize(50);
    texto4->setString(string4);
    texto4->setScale(0.50,0.50);
    sf::FloatRect textRect4 = texto4->getLocalBounds();
    texto4->setOrigin(textRect4.left, textRect4.top  + textRect4.height/2);
    texto4->setPosition(500,40);
    /**/
    
    tGenNPC = 3;
    tGenPocion = 2;
    contpoc = 0;
    numZombis = 0;
}
void Ingame::draw(){ 
    view->setCenter(pers->getPosX(), pers->getPosY());
    view2->setCenter(280, 200);
    juego->getWindow()->setView(*view);
    Mapa->dibuja(juego->getWindow());

    for(int y=0;y<c.size();y++){
        if(c[y]!=NULL){
            if(Personaje *perso = dynamic_cast<Personaje*>(c[y])){               
            }else
                juego->getWindow()->draw(*(c[y]->getSprite()));
        }
    }
    juego->getWindow()->draw(*(pers->getSprite()));
    juego->getWindow()->setView(*view2);
    if(pers!=NULL && pers && (pers->getvida()/pers->getvidamax())*100<30){
        juego->getWindow()->draw(*bajahp);
    }
    juego->getWindow()->draw(*sprite);
    juego->getWindow()->draw(*sprite2);
    juego->getWindow()->draw(*sprite3);
    if(pers->getPocion2())
        juego->getWindow()->draw(*pocion2);
    if(pers->getPocion3())
        juego->getWindow()->draw(*pocion3);
    juego->getWindow()->draw(*cara);
    
    juego->getWindow()->draw(*texto2);
    juego->getWindow()->draw(*texto3);
    juego->getWindow()->draw(*texto4);
    juego->getWindow()->draw(*balas);
    juego->getWindow()->draw(*granadas);
    
    
    if(pers!=NULL && pers && pers->getradiacion()>=50 && pers->getradiacion()<60){
        extradmg->setTextureRect(sf::IntRect(0, 0, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }else if(pers!=NULL && pers && pers->getradiacion()>=60 && pers->getradiacion()<70){
        extradmg->setTextureRect(sf::IntRect(0, 37*1, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }else if(pers!=NULL && pers && pers->getradiacion()>=70 && pers->getradiacion()<80){
        extradmg->setTextureRect(sf::IntRect(0, 37*2, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }else if(pers!=NULL && pers && pers->getradiacion()>=80 && pers->getradiacion()<90){
        extradmg->setTextureRect(sf::IntRect(0, 37*3, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }else if(pers!=NULL && pers && pers->getradiacion()>=90 && pers->getradiacion()<100){
        extradmg->setTextureRect(sf::IntRect(0, 37*4, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }else if(pers!=NULL && pers && pers->getradiacion()==100){
        extradmg->setTextureRect(sf::IntRect(0, 37*5, 96, 37));
        juego->getWindow()->draw(*extradmg);
    }
    if(noche==true)
        juego->getWindow()->draw(*texto);
    juego->getWindow()->setView(*view); 
}
void Ingame::update(){
    sprintf(string, "noche %d", nivel);
    texto->setString(string);
    sprintf(string2, "x%d",pers->getArma()->getCargador());
    texto2->setString(string2);
    sprintf(string3, "x%d", pers->getDisponibles());
    texto3->setString(string3);
    sprintf(string4, "+%d", juego->getPuntuacion());
    texto4->setString(string4);
    
    
    

    if(pers->getPocion2() && pers->getPocion3())
        pocion3->setPosition(265, 37);
    else
        pocion3->setPosition(240, 37);
    deltaTime = new sf::Time(deltaClock->restart());
    for(int u = 0; u<c.size(); u++){
        if(c[u]!=NULL){
            for(int z = 0; z<c.size(); z++){
                if(c[z]!=NULL && u!=z && c[u]->colisionar(c[z])){
                    if(Bala *bala = dynamic_cast<Bala*> (c[z])){
                        if(Npc *np = dynamic_cast<Npc*> (c[u])){
                            if(np->getEstado()){
                                np->perderVida(bala->getDanio());
                                bala->destroy();
                                c.erase(c.begin()+z);
                                c[c.size()]=NULL;
                           }
                        }
                    }
                    if(c[z]!=NULL){
                        if(Fuego *fuego = dynamic_cast<Fuego*> (c[z])){
                            if(c[u]!=NULL){
                                if(Npc *npese = dynamic_cast<Npc*> (c[u])){
                                    if(npese->getEstado()){
                                        npese->perderVida(1);
                                    }
                                }
                            }
                        }
                    }
                    if(c[z]!=NULL){
                        if(Npc *npc = dynamic_cast<Npc*> (c[z])){
                            if(c[u]!=NULL){
                                if(Personaje *pers = dynamic_cast<Personaje*> (c[u])){
                                    if(pers->getinvulnerable()==false){
                                        if(npc->getEstado()){
                                            pers->setVida(-11);
//                                            juego->sonidoGrito();
                                            pers->setinvulnerable(0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(c[z]!=NULL){
                        if(Pocion *poci = dynamic_cast<Pocion*> (c[z])){
                            if(c[u]!=NULL){
                                if(Personaje *persona = dynamic_cast<Personaje*> (c[u])){
                                    persona->cogerPocion(poci->getTipo());
                                    juego->sonidoPocion();
                                    poci->destroy();
                                    contpoc--;
                                    c.erase(c.begin()+z);
                                    c[c.size()]=NULL;
                                }
                            }
                            
                        }
                    }
                }
            }
            if(Bala *b = dynamic_cast<Bala*> (c[u])){
                if(b->update(deltaTime))
                    b->render();
                else{
                    //delete b;
                    c.erase(c.begin()+u);
                    c[c.size()]=NULL;
                }
            }
            else if(Bomba *bo = dynamic_cast<Bomba*> (c[u])){
                if(bo->update(&c))
                    bo->render();
                else{
                    //delete bo;
                    c.erase(c.begin()+u);
                    c[c.size()]=NULL;
                }
            }
            else if(Fuego *f = dynamic_cast<Fuego*> (c[u])){
                if(f->update(&c))
                    f->render();
                else{
                    //delete f;
                    c.erase(c.begin()+u);
                    c[c.size()]=NULL;
                }
            }
            else if(Npc *n = dynamic_cast<Npc*> (c[u])){
                if(n->update(deltaTime, pers->getPosX(), pers->getPosY() ,&c, &p)){
                    n->render(seconds);
                }
                else{
                    numZombis--;
                    muertos++;
                    c.erase(c.begin()+u);
                    c[c.size()]=NULL;
                }
            }
            else if(Personaje *pj = dynamic_cast<Personaje*> (c[u])){
               if(pj->update(seconds, textura, &c, &p, deltaTime)){
                    pj->render(seconds);
               }
                else{
                    gameover=true;
                }
            }
            else if(Pocion *poc = dynamic_cast<Pocion*> (c[u])){
                poc->render(seconds);
            }
            else{
                c[u]->destroy();
                c.erase(c.begin()+u);
                c[c.size()]=NULL;
            }
        }
    }
    for(int u = 0; u<p.size(); u++){
            if(p[u]!=NULL){
                for(int z = 0; z<c.size(); z++){
                    if(c[z]!=NULL && u!=z && p[u]->colisionar(c[z])){
                        if(Bala *b = dynamic_cast<Bala*> (c[z])){
                            b->destroy();
                            c.erase(c.begin()+z);
                            c[c.size()]=NULL;
                        }
                        if(c[z]!=NULL){
                            if(Pocion *poc = dynamic_cast<Pocion*> (c[z])){
                                poc->destroy();
                                contpoc--;
                                c.erase(c.begin()+z);
                                c[c.size()]=NULL;
                            }
                        }
                    }
                }
            }
        }
    generarNpc();
    pers->getArma()->update();
    if(((pers->getvida()/pers->getvidamax())*100) > 90){
        sprite->setTextureRect(sf::IntRect(0*32, 10*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100) > 80){
        sprite->setTextureRect(sf::IntRect(0*32, 9*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>70){
        sprite->setTextureRect(sf::IntRect(0*32, 8*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>60){
        sprite->setTextureRect(sf::IntRect(0*32, 7*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>50){
        sprite->setTextureRect(sf::IntRect(0*32, 6*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>40){
        sprite->setTextureRect(sf::IntRect(0*32, 5*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>30){
        sprite->setTextureRect(sf::IntRect(0*32, 4*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>20){
        sprite->setTextureRect(sf::IntRect(0*32, 3*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>10){
        sprite->setTextureRect(sf::IntRect(0*32, 2*32, 7*32, 32));
    }else if (((pers->getvida()/pers->getvidamax())*100)>0){
        sprite->setTextureRect(sf::IntRect(0*32, 1*32, 7*32, 32));
    }else if ((pers->getvida())==0){
        sprite->setTextureRect(sf::IntRect(0*32, 0*32, 7*32, 32));
    }
   

     
    if((pers->getradiacion()>90)){
        sprite2->setTextureRect(sf::IntRect(8*32, 10*32, 5*32, 32));
    }else if ((pers->getradiacion()>80)){
        sprite2->setTextureRect(sf::IntRect(8*32, 9*32, 5*32, 32));
    }else if (((pers->getradiacion())>70)){
        sprite2->setTextureRect(sf::IntRect(8*32, 8*32, 5*32, 32));
    }else if ((pers->getradiacion()>60)){
        sprite2->setTextureRect(sf::IntRect(8*32, 7*32, 5*32, 32));
    }else if ((pers->getradiacion()>50)){
        sprite2->setTextureRect(sf::IntRect(8*32, 6*32, 5*32, 32));
    }else if ((pers->getradiacion()>40)){
        sprite2->setTextureRect(sf::IntRect(8*32, 5*32, 5*32, 32));
    }else if ((pers->getradiacion()>30)){
        sprite2->setTextureRect(sf::IntRect(8*32, 4*32, 5*32, 32));
    }else if ((pers->getradiacion()>20)){
        sprite2->setTextureRect(sf::IntRect(8*32, 3*32, 5*32, 32));
    }else if ((pers->getradiacion()>10)){
        sprite2->setTextureRect(sf::IntRect(8*32, 2*32, 5*32, 32));
    }else if ((pers->getradiacion())>0){
        sprite2->setTextureRect(sf::IntRect(8*32, 1*32, 5*32, 32));
    }else if ((pers->getradiacion()==0)){
        sprite2->setTextureRect(sf::IntRect(8*32, 0*32, 5*32, 32));
    }
    if(gameover)
        gameOver();
}

bool Ingame::getIniciado(){
    return iniciado;
}

bool Ingame::getPausa(){
    return pausa;
}

void Ingame::setPausa(){
    pausa = !pausa;
    if(pausa){
        delete deltaTime;
        delete deltaClock;
        juego->getWindow()->setView(juego->getWindow()->getDefaultView());
    }
    if(!pausa){
        deltaClock = new sf::Clock();
    }
}

void Ingame::generarNpc(){
    float posXx=0;
    float posY=0;
    int eleccion = rand() %7;
    tipoNPC = rand()%3;
    time = new sf::Time(clock->getElapsedTime());
    seconds = time->asSeconds();
    if(seconds-tGenNPC>=2 && ronda<(30+nivel*5)){
        noche=false;
        if((nivel%3)==0 && bossCreado == false){
            c.push_back(new Npc(45, 45, 80, posY, textura, dificultad, 4));
            bossCreado = true;
        }
        if(eleccion==0){
            posXx=402;
            posY=-10;
        }else if(eleccion==1){
            posXx=1300;
            posY=-10;
        }else if(eleccion==2){
            posXx=1610;
            posY=300;
        }else if(eleccion==3){
            posXx=1610;
            posY=1150;
        }else if(eleccion==4){
            posXx=950;
            posY=1610;
        }else if(eleccion==5){
            posXx=-10;
            posY=1237;
        }else if(eleccion==6){
            posXx=-10;
            posY=390;
        }
        c.push_back(new Npc(15,15,posXx,posY, textura, dificultad, tipoNPC));
        tGenNPC=seconds;
        if(numZombis>=5){
            juego->sonidoZombis(5);
        }
        else{
            juego->sonidoZombis(numZombis);
        }
    }
    cambioRondaTime = new sf::Time(cambioRondaClock->getElapsedTime());
    tiempoRonda = cambioRondaTime->asSeconds();
    rondaTime = new sf::Time(clockRonda->getElapsedTime());
    ronda = rondaTime->asSeconds();
    if(tiempoRonda>=0 && tiempoRonda<3)
        noche = true;
    if(ronda>=(45+nivel*5)){
        dificultad += (log10(nivel+3)/log10(3.0))-(log10(nivel+2)/log10(3.0))+0.5;
        bossCreado = false;
        nivel++;
        cambioRondaClock->restart();
        clockRonda->restart();
        ronda = 0;
        tiempoRonda = 0;
    }
    float posX = rand() %Mapa->getWidth();
    float posY2 = rand() %Mapa->getHeight();
    float tipo = rand() %3;
    if(seconds-tGenPocion>=5 && contpoc<30){
        c.push_back(new Pocion(10, 10, tipo+1, posX, posY2, textura));
        contpoc++;
        tGenPocion=seconds;
    }
}
Ingame::~Ingame() {
}

void Ingame::destroy(){
    delete this;
}

void Ingame::gameOver(){
    for(int u = 0; u<c.size(); u++){
        if(Bala *b = dynamic_cast<Bala*> (c[u])){
            b->destroy();
            c[u]=NULL;
        }
        else if(Bomba *bo = dynamic_cast<Bomba*> (c[u])){
            bo->destroy();
            c[u]=NULL;
        }
        else if(Fuego *f = dynamic_cast<Fuego*> (c[u])){
            f->destroy();
            c[u]=NULL;
        }
        else if(Npc *n = dynamic_cast<Npc*> (c[u])){
            n->destroy();
            c[u]=NULL;
        }
        else if(Pocion *poc = dynamic_cast<Pocion*> (c[u])){
            poc->destroy();
            c[u]=NULL;
        }
        else if(Personaje *pers = dynamic_cast<Personaje*>(c[u])){
            pers->destroy();
            c[u]=NULL;
        }
        else{
            c[u]->destroy();
            c[u]=NULL;
        }
    }
    c.clear();
    delete textura;
    delete texturahudbars;
    delete texturahud;
    delete bajavida;
    delete texturadmg;
    delete deltaTime;
    delete cambioRondaTime;
    delete time;
    delete sprite;
    delete view;
    delete view2;
    delete cara;
    delete sprite2;
    delete sprite3;
    delete bajahp;
    delete extradmg;
    delete pocion2;
    delete pocion3;
    juego->setPersonaje(0);
    //delete pers;
    iniciado = false;
    pausa = true;
    juego->getWindow()->setView(juego->getWindow()->getDefaultView());
    return juego->setState(3, -1);
}