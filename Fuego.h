/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fuego.h
 * Author: Raquel
 *
 * Created on 9 de mayo de 2016, 16:41
 */

#ifndef FUEGO_H
#define FUEGO_H

#include <SFML/Graphics.hpp>
#include "Colisionable.h"
#include <math.h>


class Fuego: public Colisionable{
public:
    Fuego();
    Fuego(int bx, int by, float x, float y, double mouseX, double mouseY, sf::Texture *tex, int boundx);
    ~Fuego();
    sf::Sprite getSprite();
    void destroy();
    bool update(std::vector<Colisionable*>* c);
    void render();
private:
    float posX;
    float posY;
    float dirx;
    float diry;
    
    float vida;
    int numframe;
    float time;
    float timeExpl;
    
    sf::Clock *clock2;
    sf::Time *elapsed;
};

#endif /* FUEGO_H */
