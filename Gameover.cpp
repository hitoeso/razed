/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Gameover.cpp
 * Author: Miguel y Luis
 * 
 * Created on 10 de mayo de 2016, 16:30
 */

#include "Gameover.h"

Gameover* Gameover::pinstance = 0;

Gameover* Gameover::Instance(){
    if(pinstance == 0){
        pinstance = new Gameover();
    }
    return pinstance;
}

Gameover::Gameover() {
    juego = Juego::Instance();
    tFondo0 = new sf::Texture;
    if (!tFondo0->loadFromFile("resources/gameover.png"))
    {
        std::cerr << "Error cargando la imagen gameover.png";
        exit(0);
    }
}

void Gameover::setStatus(int p, float sx, float sy){
    fondo = new sf::Sprite(*tFondo0);
    fondo->setOrigin(600/2,400/2);
    fondo->setTextureRect(sf::IntRect(0, 0, 600, 400));
    fondo->setPosition(800*sx/2, 480*sy/2);
    fondo->scale(sx, sy);
    clock = new sf::Clock();
    time = 0;
    timead = 0;
    cont=0;
    char string[50];
    sprintf(string, "PUNTUACION: %d", p);
    fuente = new sf::Font();
    if(!fuente->loadFromFile("resources/upheavtt.ttf"))
        printf("Error cargando la fuente seleccionada");
    texto = new sf::Text();
    texto->setFont(*fuente);
    texto->setColor(sf::Color::White);
    texto->setCharacterSize(200);
    texto->setString(string);
    sf::FloatRect textRect = texto->getLocalBounds();
    texto->setOrigin(textRect.left + textRect.width/2, textRect.top  + textRect.height/2);
    texto->setPosition(400*sx,350*sy);
    texto->setScale(0.25, 0.25);
    std::cout<<"Puntuacion desde setStatus "<<p<<std::endl;
}

void Gameover::draw(){
    juego->getWindow()->draw(*fondo);
    juego->getWindow()->draw(*texto);
}

void Gameover::update(){
    elapsed = new sf::Time(clock->getElapsedTime());
    time=elapsed->asSeconds();
    if(time-timead>0.5){
        if(cont==0){
            fondo->setTextureRect(sf::IntRect(0, 0, 600, 400));
        }
        else if(cont==1){
            fondo->setTextureRect(sf::IntRect(0, 400, 600, 400));
        }
        else if(cont==2){
            fondo->setTextureRect(sf::IntRect(600, 0, 600, 400));
        }
        else if(cont==3){
            fondo->setTextureRect(sf::IntRect(600, 400, 600, 400));
        }
        cont++;
        if(cont>3)
            cont = 0;
        timead=time;
    }
    
}

Gameover::~Gameover() {
}

