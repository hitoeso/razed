/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Juego.cpp
 * Author: Raquel
 * 
 * Created on 18 de abril de 2016, 14:28
 */


#include "Juego.h"
#include "Menu.h"
#include "Intro.h"
#include "Ingame.h"
#include "Bala.h"
#include "Gameover.h"
#define sx sf::VideoMode::getDesktopMode().width/800.0
#define sy sf::VideoMode::getDesktopMode().height/480.0

Juego* Juego::pinstance = 0;

Juego* Juego::Instance(){
    if(pinstance == 0){
        pinstance = new Juego;
    }
    return pinstance;
}

void Juego::inicializarJuego() {
    puntuacion = 0;
    personaje=0;
    arma=0;
    this->setState(1, 0);
}
Juego::Juego() {
    bSonido = true;
    musica = new sf::Music;
    musica->openFromFile("resources/intro.ogg");
    musica->setLoop(true);
    //musica->play();
    
    sf::SoundBuffer *buffer = new sf::SoundBuffer;
    buffer->loadFromFile("resources/sound.ogg");
    sonido = new sf::Sound;
    sonido->setBuffer(*buffer);
    sonido->play();
    sonido->setVolume(100);
    
    sf::SoundBuffer *buffer2 = new sf::SoundBuffer;
    buffer2->loadFromFile("resources/disparo.ogg");
    disparo = new sf::Sound;
    disparo->setBuffer(*buffer2);
    
    sf::SoundBuffer *buffer3 = new sf::SoundBuffer;
    buffer3->loadFromFile("resources/granada.ogg");
    granada = new sf::Sound;
    granada->setBuffer(*buffer3);
    
    sf::SoundBuffer *buffer4 = new sf::SoundBuffer;
    buffer4->loadFromFile("resources/fireinthehole.ogg");
    fireinthehole = new sf::Sound;
    fireinthehole->setBuffer(*buffer4);
    
    sf::SoundBuffer *buffer5 = new sf::SoundBuffer;
    buffer5->loadFromFile("resources/zombis.ogg");
    zombis = new sf::Sound;
    zombis->setBuffer(*buffer5);
    
    sf::SoundBuffer *buffer6 = new sf::SoundBuffer;
    buffer6->loadFromFile("resources/recarga.ogg");
    recarga = new sf::Sound;
    recarga->setBuffer(*buffer6);  
    
    sf::SoundBuffer *buffer7 = new sf::SoundBuffer;
    buffer7->loadFromFile("resources/pocion.ogg");
    pocion = new sf::Sound;
    pocion->setBuffer(*buffer7); 
    
    sf::SoundBuffer *buffer8 = new sf::SoundBuffer;
    buffer8->loadFromFile("resources/grito.ogg");
    grito = new sf::Sound;
    grito->setBuffer(*buffer8); 
    
    sf::Texture *raton = new sf::Texture();
    raton->loadFromFile("resources/mirilla.png");
    ratonspr = new sf::Sprite();
    ratonspr->setTexture(*raton);
    
    window = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Razed", sf::Style::Fullscreen);
    //window = new sf::RenderWindow(sf::VideoMode(800, 480), "Razed");
    
    window->setMouseCursorVisible(false);
    //std::cout<<ratonspr.getOrigin().x<<" "<<ratonspr.getOrigin().y<<std::endl;
    ratonspr->setOrigin(64, 64);
}

void Juego::setState(int i, int w){
    if(i == 0){
    }
    if(i == 1){
        estadoJuego = Menu::Instance();
        setMenu(w);
    }
    if(i == 2){
       estadoJuego = Ingame::Instance();
       Ingame *j = dynamic_cast<Ingame*> (estadoJuego);
       puntuacion = 0;
       j->setPausa();
       setIngame();
    }
    if(i == 3){
        estadoJuego = Gameover::Instance();
        Gameover *g = dynamic_cast<Gameover*> (estadoJuego);
        //j->destroy();
        setGameover();
    }
}
/*Inicializamos el Menu*/
void Juego::setMenu(int j){    
    if(Menu *m = dynamic_cast<Menu*> (estadoJuego)){
        m->setStatus(j, sx, sy);
    }
}

void Juego::setGameover(){    
    if(Gameover *go = dynamic_cast<Gameover*> (estadoJuego)){
        go->setStatus(puntuacion, sx, sy);
        std::cout<< "llego aquiiioioioiiii" << std::endl;
    }
}

void Juego::setIngame(){ 
    if(Ingame *j = dynamic_cast<Ingame*> (estadoJuego)){
        sf::Texture *sprites = new sf::Texture;
        sf::Texture *spriteshud = new sf::Texture;
        sf::Texture *spriteshudbars = new sf::Texture;
        sf::Texture *bajavida = new sf::Texture;
        sf::Texture *extradamage = new sf::Texture;
        sf::Font *fuente = new sf::Font;
            
        if(!fuente->loadFromFile("resources/AlegreyaSans-ExtraBold.ttf")){
            printf("Error cargando la fuente seleccionada");
        }
        if (!sprites->loadFromFile("resources/soldado.png"))
        {
            std::cerr << "Error cargando la imagen sprites.png";
            exit(0);
        }
        if (!spriteshudbars->loadFromFile("resources/hudsheetbars.png"))
        {
            std::cerr << "Error cargando la imagen spriteshudbars.png";
            exit(0);
        }
        if (!spriteshud->loadFromFile("resources/hudsheet.png"))
        {
            std::cerr << "Error cargando la imagen spriteshud.png";
            exit(0);
        }
        if (!bajavida->loadFromFile("resources/lowlife.png"))
        {
            std::cerr << "Error cargando la imagen spriteshud.png";
            exit(0);
        }
        if (!extradamage->loadFromFile("resources/dmg.png"))
        {
            std::cerr << "Error cargando la imagen spriteshud.png";
            exit(0);
        }
        if(!j->getIniciado()){
            j->setStatus(sprites, spriteshudbars,spriteshud, bajavida, extradamage, fuente);
        }
    }
}

/*Inicializamos el juego*/
void Juego::run(){
    if(Menu *m = dynamic_cast<Menu*> (estadoJuego)){
        m->update(sx, sy);
    }
    else if(Gameover *g = dynamic_cast<Gameover*> (estadoJuego)){
        //std::cout << "creado hago update " <<std::endl;
        g->update();
        std::cout<<"UPDATE DE GAMEOVER"<<std::endl;
    }
    else if(Ingame *j = dynamic_cast<Ingame*> (estadoJuego)){

        if(!j->getPausa())
            j->update();
    }

    //Bucle de obtención de eventos
    sf::Event *event = new sf::Event;
    while (window->pollEvent(*event))
    {

        switch(event->type){

            //Si se recibe el evento de cerrar la ventana la cierro
            case sf::Event::Closed:
                window->close();
                break;
            case sf::Event::MouseButtonPressed:
                switch(event->mouseButton.button){
                    case sf::Mouse::Left:
                        if(Menu *m = dynamic_cast<Menu*> (estadoJuego)){
                            m->click(sx, sy);
                        }
                        break;
                }
                break;
            //Se pulsó una tecla, imprimo su codigo
            case sf::Event::KeyPressed:

                //Verifico si se pulsa alguna tecla de movimiento
                switch(event->key.code) {

                    //Tecla ESC para salir
                    case sf::Keyboard::Escape:
                        window->close();
                    break;
                    case sf::Keyboard::P:
                        if(Ingame *ingame = dynamic_cast <Ingame*>(estadoJuego)){
                            this->setState(1,1);
                            ingame->setPausa();
                        }
                    break;
                    case sf::Keyboard::Space:
                        if(Gameover *gameover = dynamic_cast <Gameover*>(estadoJuego)){
                            this->setState(1,0);
                            std::cout<<"HOOOOLA"<<std::endl;
                        }
                    break;
                    //Cualquier tecla desconocida se imprime por pantalla su código
                    default:
                    break;

                }
        }

    }
    window->clear();
    
    ratonspr->setPosition(static_cast<sf::Vector2f>(sf::Mouse::getPosition(*window)));
    if(Menu *m = dynamic_cast<Menu*> (estadoJuego)){
       ratonspr->setScale(0.75, 0.75);
        m->draw();
    }
    if(Ingame *j = dynamic_cast<Ingame*>(estadoJuego)){
        if(!j->getPausa()){
            ratonspr->setScale(0.15, 0.15);
            ratonspr->setPosition(static_cast<sf::Vector2f>(window->mapPixelToCoords(sf::Mouse::getPosition(*window))));
            j->draw();
        }
    }
    
    window->draw(*ratonspr);
    if(Gameover *g = dynamic_cast<Gameover*>(estadoJuego)){
        g->draw();
    }
    window->display();
}

double Juego::getMouseX(){
    sf::Vector2f position = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
    return position.x;
}
double Juego::getMouseY(){
    sf::Vector2f position = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
    return position.y;
}

int Juego::getPuntuacion(){
    return puntuacion;
}
void Juego::cambiarMusica(std::string cancion){
    musica->openFromFile("resources/"+cancion);
    musica->setLoop(true);
    musica->play();
}
void Juego::setMusica(){
    if(musica->getStatus() == sf::Music::Paused)
        musica->play();
    else
        musica->pause();
}
void Juego::setSonido(){
    if(!bSonido){
        sonido->play();
        bSonido = true;
    }
    else{
        sonido->stop();
        bSonido = false;
    }
}
void Juego::playSonido(){
     if(bSonido){
        sonido->play();
    }
}
void Juego::setVolMusica(float vol){
    musica->setVolume(vol);
}
void Juego::setVolSonido(float vol){
    sonido->setVolume(vol);
    disparo->setVolume(vol);
    granada->setVolume(vol);
    fireinthehole->setVolume(vol);
    zombis->setVolume(vol);
    recarga->setVolume(vol);
    pocion->setVolume(vol);
}

void Juego::setPersonaje(int i){
    personaje = i;
}

void Juego::setArma(int i){
    arma = i;
}
void Juego::setPuntuacion(int i){
    puntuacion+=i;
}
int Juego::getPersonaje(){
    return personaje;
}

int Juego::getArma(){
    return arma;
}

bool Juego::getMusica(){
    if(musica->getStatus() == sf::Music::Paused){
        return false;
    }
    else{
        return true;
    }
}
bool Juego::getSonido(){
    return bSonido;
}

float Juego::getVolMusica(){
    return musica->getVolume();
}
float Juego::getVolSonido(){
    return sonido->getVolume();
}

void Juego::sonidoDisparo(){
    disparo->play();
}

void Juego::sonidoGranada(){
    granada->play();
}

void Juego::sonidoZombis(int numZombis){
    zombis->setVolume(numZombis*5);
    if(zombis->getStatus() == sf::Sound::Stopped || zombis->getStatus() == sf::Sound::Paused)
        zombis->play();
}

void Juego::sonidoFireinthehole(){
    fireinthehole->play();
}

void Juego::sonidoRecarga(){
    recarga->play();
}

void Juego::sonidoPocion(){
    pocion->play();
}

void Juego::sonidoGrito(){
    grito->play();
}

sf::RenderWindow* Juego::getWindow(){
    return window;
}

void Juego::destroy() {
    delete this;
}
Juego::~Juego() {
    
}
