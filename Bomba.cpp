/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bomba.cpp
 * Author: Raquel
 * 
 * Created on 14 de marzo de 2016, 15:04
 */

#include "Bomba.h"
#include <math.h>
#include <iostream>


#include "Npc.h"

Bomba::Bomba() {
}

Bomba::Bomba(int bx, int by, float x, float y, double mouseX, double mouseY, sf::Texture *tex) : Colisionable(bx, by) {
    
    clock2 = new sf::Clock;
    
    sprite = new sf::Sprite(*tex);
    sprite->setOrigin(32/2, 32/2);
    sprite->setTextureRect(sf::IntRect(32, 32*14, 32, 32));
    sprite->setScale(0.35, 0.35);
    sprite->setPosition(x, y);
    
    float angx = mouseX-x;
    float angy = mouseY-y;
    float mod = sqrt(angy*angy+angx*angx);
    dirx = angx/mod;
    diry = angy/mod; 
    
    posX = x;
    posY = y;
    
    inicialx=x;
    inicialy=y;
    
    vel=125;
    vida=1;
    time = 0; 
    timeExpl=0;
    numframe = 0;
}

Bomba::~Bomba() {
}

bool Bomba::update(std::vector<Colisionable*>* c){
    elapsed = new sf::Time(clock2->getElapsedTime());
    time = elapsed->asSeconds();
    delete elapsed;
    if(time < vida){
        if(time < 0.4){
            sprite->scale(1.0004f, 1.0004f);
        }
        else if(time > 0.6){
            sprite->scale(1/1.0004f, 1/1.0004f);
        }
        posX = inicialx+(vel*time)*dirx;
        posY = inicialy+(vel*time)*diry;
        return true;
    }
    else if(numframe < 8){
        if(time-timeExpl>=0.1){
            sprite->setOrigin(64/2, 64/2);
            sprite->setTextureRect(sf::IntRect((numframe*2)*32, 32*12, 64, 64));
            sprite->setScale(2, 2);
            numframe++;
            timeExpl = time;
            
             for(int z = 0; z<c->size(); z++){
                    if(c->at(z)!=NULL){
                        if(Npc *n = dynamic_cast<Npc*> (c->at(z))){
                            if((abs(((int)posX-n->getPosX()))+abs(((int)posY-n->getPosY())))<60){    
                                n->perderVida(50);
                            }
                        }
                    }
                }
        }
        return true;
    }
    else{
        this->destroy();
        return false; 
    }
}

void Bomba::render(){
    sprite->setPosition(posX,posY);
}

sf::Sprite Bomba::getSprite(){
    return *sprite;
}

void Bomba::destroy() {
    delete clock2;
    delete sprite;
    delete this;
}

