/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Gameover.h
 * Author: Miguel y Luis
 *
 * Created on 10 de mayo de 2016, 16:30
 */

#ifndef GAMEOVER_H
#define GAMEOVER_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include "vector"
#include <cstddef>
#include <string>

#include "Estado.h"
#include "Juego.h"


class Gameover : public Estado{
public:
    static Gameover* Instance();
    void setStatus(int p, float sx, float sy);
    void draw();
    void update();
protected:
    Gameover();
    virtual ~Gameover();
    void destroy();
private:
    static Gameover* pinstance;
    Juego *juego;
    sf::Sprite* fondo;
    sf::Texture *tFondo0;
    float time;
    float timead;
    sf::Clock *clock;
    sf::Time *elapsed;
    int cont;
    
    sf::Font *fuente;
    sf::Text *texto;
};

#endif /* GAMEOVER_H */

