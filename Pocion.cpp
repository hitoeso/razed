/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pocion.cpp
 * Author: Carmina
 * 
 * Created on 6 de mayo de 2016, 11:42
 */

#include "Pocion.h"

Pocion::Pocion(int bx, int by, int tipoPocion, float x, float y, sf::Texture *tex) : Colisionable(bx, by){
    tipo = tipoPocion;
    times4 = 0;
    contPocion = 1;
    sentidoPocion = 1;
    sprite = new sf::Sprite(*tex);
    sprite->setOrigin(25/2, 28/2);
    sprite->setTextureRect(sf::IntRect(1*32, (7+tipo)*32, 25, 28));
    sprite->setPosition(x, y);
    sprite->scale(0.7,0.7);

}

sf::Sprite Pocion::getSprite(){
    return *sprite;
}

void Pocion::destroy(){
    delete sprite;
    delete this;
}

void Pocion::render(float sec){
    if(sec-times4>0.15){
        sprite->setTextureRect(sf::IntRect(contPocion*32, (7+tipo)*32, 25, 28));
        if(sentidoPocion==1){
            contPocion++;
            sprite->move(0,-1);
        }
        if(sentidoPocion==-1){
            contPocion--;
            sprite->move(0,1);
        }
        if(contPocion == 7 || contPocion == 1) sentidoPocion=sentidoPocion*-1;

        times4=sec;
        
        
    }
}

int Pocion::getTipo(){
    return tipo;
}

Pocion::Pocion() {
}

Pocion::~Pocion() {
}

