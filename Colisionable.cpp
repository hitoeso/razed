/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Colisionable.cpp
 * Author: Raquel
 * 
 * Created on 16 de marzo de 2016, 16:00
 */

#include "Colisionable.h"

Colisionable::Colisionable() {
}

Colisionable::Colisionable(int bx, int by) {
    boundX = bx;
    boundY = by; 
}

Colisionable::~Colisionable() {
    
}
bool Colisionable::colisionar(Colisionable *obj){
   
    if (
            (((obj->getSprite()->getPosition().x-obj->getBounding().x) < (sprite->getPosition().x+boundX) 
            && (obj->getSprite()->getPosition().x+obj->getBounding().x) > (sprite->getPosition().x+boundX)) 
            || ((obj->getSprite()->getPosition().x-obj->getBounding().x) < (sprite->getPosition().x-boundX) 
            && (obj->getSprite()->getPosition().x+obj->getBounding().x) > (sprite->getPosition().x-boundX)))
            || (((obj->getSprite()->getPosition().x+obj->getBounding().x) < (sprite->getPosition().x+boundX) 
            && (obj->getSprite()->getPosition().x+obj->getBounding().x) > (sprite->getPosition().x-boundX)) 
            || ((obj->getSprite()->getPosition().x-obj->getBounding().x) < (sprite->getPosition().x+boundX) 
            && (obj->getSprite()->getPosition().x-obj->getBounding().x) > (sprite->getPosition().x-boundX)))
        )
    {
        if (
                (((obj->getSprite()->getPosition().y-obj->getBounding().y) < (sprite->getPosition().y+boundY) 
                && (obj->getSprite()->getPosition().y+obj->getBounding().y) > (sprite->getPosition().y+boundY)) 
                || ((obj->getSprite()->getPosition().y-obj->getBounding().y) < (sprite->getPosition().y-boundY) 
                && (obj->getSprite()->getPosition().y+obj->getBounding().y) > (sprite->getPosition().y-boundY)))
                || (((obj->getSprite()->getPosition().y+obj->getBounding().y) < (sprite->getPosition().y+boundY) 
                && (obj->getSprite()->getPosition().y+obj->getBounding().y) > (sprite->getPosition().y-boundY)) 
                || ((obj->getSprite()->getPosition().y-obj->getBounding().y) < (sprite->getPosition().y+boundY) 
                && (obj->getSprite()->getPosition().y-obj->getBounding().y) > (sprite->getPosition().y-boundY)))
            )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void Colisionable::destroy() {
    delete this;
}

sf::Sprite* Colisionable::getSprite(){
    return sprite;
}
sf::Vector2i Colisionable::getBounding(){
    return sf::Vector2i(boundX, boundY);
}
float Colisionable::getPosX() {
    return sprite->getPosition().x;
}
float Colisionable::getPosY() {
    return sprite->getPosition().y;
}