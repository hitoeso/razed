/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Juego.h
 * Author: Raquel
 *
 * Created on 18 de abril de 2016, 14:28
 */

#ifndef JUEGO_H
#define JUEGO_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Estado.h"

class Juego {
public:
    static Juego* Instance();
    void inicializarJuego();
    /*Metodo que devuelve la ventana*/
    sf::RenderWindow* getWindow();
    /**/
    /*Metodo que devuelve las coordenadas del raton*/
    double getMouseX();
    double getMouseY();
    /**/
    /*Métodos relacionados con el sonido y la música*/
    void setMusica();
    void setSonido();    
    void setVolSonido(float vol);
    void setVolMusica(float vol);
    void setPuntuacion(int i);
    bool getMusica();
    bool getSonido();
    float getVolMusica();
    float getVolSonido();
    int getPuntuacion();
    void playSonido();
    void sonidoDisparo();
    void sonidoGranada();
    void sonidoFireinthehole();
    void sonidoZombis(int numZombis);
    void sonidoRecarga();
    void sonidoPocion();
    void sonidoGrito();
    void cambiarMusica(std::string cancion);
    /**/
    
    /*Metodos de seleccion de personaje y arma*/
    void setPersonaje(int);
    void setArma(int);
    int getPersonaje();
    int getArma();
    
    /**/
    void run();
    
    void setState(int i, int j); //i.0: Intro; i.1: Menu; i.2:InGame; i.3:Fin; 
                                 //j.0: Menu Inicio; j.1: Menu Pausa; j.2: Opciones; j.3:Seleccion de Personaje
    
protected:
    Juego();
    virtual ~Juego();
    void setMenu(int j);
    void setIngame();
    void setGameover();
    void destroy();
private:
    static Juego* pinstance;
    
    Estado* estadoJuego;
    sf::RenderWindow* window;
    sf::Music* musica;
    bool bSonido;
    sf::Sound* sonido;
    sf::Sound* disparo;
    sf::Sound* granada;
    sf::Sound* fireinthehole;
    sf::Sound* zombis;
    sf::Sound* recarga;
    sf::Sound* pocion;
    sf::Sound* grito;
    sf::Sprite *ratonspr;
    int volumenS;
    int volumenM;
    
    int personaje;
    int arma;
    
    sf::Font *fuente;
    
    int puntuacion;
};

#endif /* JUEGO_H */

