/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pared.h
 * Author: Carmina
 *
 * Created on 20 de abril de 2016, 15:11
 */

#ifndef PARED_H
#define PARED_H
#include <SFML/Graphics.hpp>
#include "Colisionable.h"
#include <math.h>
#include <cmath>

class Pared: public Colisionable{
public:
    Pared();
    Pared(float bx, float by, float x, float y, float w, float h, float rot);
    sf::Sprite* getSprite();
    virtual ~Pared();
    bool update();
    std::vector<float> calculaPuntos();
    float getWidth();
    float getHeight();
    float getPX();
    float getPY();
private:
    float px;
    float py;
    float width;
    float height;
};

#endif /* PARED_H */

